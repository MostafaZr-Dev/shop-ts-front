
#  Shop Project With Typescript
shop project with ** express - typescript - react - nextjs **

**admin-panel**
[panel-repo](https://gitlab.com/MostafaZr-Dev/shop-ts-panel)
**server**
[server-repo](https://gitlab.com/MostafaZr-Dev/shop-ts-server)

![enter image description here](https://s4.uupload.ir/files/shop-front-ts_xfm.jpg)

![enter image description here](https://s4.uupload.ir/files/shop-front-ts3_i2g1.jpg)

![enter image description here](https://s4.uupload.ir/files/shop-front-ts4_dkq6.jpg)

![enter image description here](https://s4.uupload.ir/files/shop-front-ts5_iraz.jpg)
##  Technologies

  
### server
- Express
- Multer
- MongoDB(Mongoose)
- Redis
### admin-panel
- react
- react-router
- context-api
- material-ui
- axios
### front
- nextjs
- context-api
- clab-template
  

##  Features

- manage products ( create | update | delete )
- manage categories ( create-category with attributes )
- products list
- shopping cart 
- coupon
- payment ( zarinpal )
- notification ( sms - email )
- settings
