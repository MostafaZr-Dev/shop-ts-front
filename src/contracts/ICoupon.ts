export default interface ICoupon {
    code: string;
    percent: number;
}