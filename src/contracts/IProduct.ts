import ICategory from "./ICategory";
import IProductAttributeGroup from "./IProductAttributeGroup";


type ProductVariationItem = {
    title: string
    value: string
}

type ProductVariation = {

    title: string;
    type: string;
    name: string;
    items: ProductVariationItem[]

}

type PriceVariation = {
    price: number;
    items: object[];
}

export default interface IProduct {

    id: string;
    title: string;
    slug: string;
    thumbnail: string;
    category: ICategory;
    gallery: string[];
    price: number;
    discountedPrice: number;
    priceVariations: PriceVariation[];
    variations: ProductVariation[];
    stock: number;
    attributes: IProductAttributeGroup[]

}