

type Attributes = {
    title: string;
    slug: string;
}

type CategoryGroup = {
    title: string;
    slug: string;
    attributes: Attributes[]
}

export default interface ICategory {
    id: string;
    title: string;
    slug: string;
    groups: CategoryGroup[];

}