import AdviceToBuy from "./CommentAdviceToBuy";
import CommentStatus from "./CommentStatus";

type IUser = {
    firstName: string;
    lastName: string;
    avatar: string;
}

export default interface IComment {
    id: string;
    user: IUser;
    product: string;
    title: string;
    body: string;
    createdAt: string;
    isBuyer: boolean;
    adviceToBuy: AdviceToBuy;
    status: CommentStatus;
}