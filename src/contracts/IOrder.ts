import OrderStatus from "./OrderStatus";

type OrderLines = {
    product: object;
    price: number;
    discountedPrice: number;
    quantity: number;
    createdAt: string;
}

export default interface IOrder {

    id: string;
    totalPrice: number;
    finalPrice: number;
    orderLines: OrderLines[];
    deliveryAddress: string;
    createdAt: string;
    updatedAt: string;
    status: OrderStatus;

}