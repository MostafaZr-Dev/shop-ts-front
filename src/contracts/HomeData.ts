import IProduct from "./IProduct";

export default interface HomeData {
    newests: IProduct[],
    bestSellers: IProduct[],
    populars: IProduct[],
    mostViewed: IProduct[]
}