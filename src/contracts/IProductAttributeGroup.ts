import IProductAttribute from "./IProductAttribute";

export default interface IProductAttributeGroup {

    title: string;
    attributes: IProductAttribute[];
}