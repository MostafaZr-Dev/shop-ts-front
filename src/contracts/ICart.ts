

export default interface ICart {
    productID: string;
    productTitle: string;
    productThumbnail: string;
    price: number;
    discountedPrice: number;
    quantity: number;

}