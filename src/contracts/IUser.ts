import IAddress from "./IAddress";

export default interface IUser {
    id: string;
    firstName: string;
    lastName: string;
    fullName: string;
    email: string;
    mobile: string;
    totalOrders: number;
    wallet: number;
    addresses: IAddress[];
    createAt: string;
}