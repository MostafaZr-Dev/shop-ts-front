import React, { createContext } from 'react'

import Action from '@contracts/Action'
import { AppState } from './initState'

type AppContextProps = {
    state: AppState;
    dispatch: React.Dispatch<Action>
}

export default createContext<AppContextProps>({} as AppContextProps)