import React, { useReducer } from 'react'

import AppContext from './appContext'
import InitState from './initState'
import AppReducer from './reducer'

const AppProvider = ({ children }: React.PropsWithChildren<{}>) => {

    const [state, dispatch] = useReducer(AppReducer,InitState)

    return (
        <AppContext.Provider value={{state,dispatch}}>
            {children}
        </AppContext.Provider>
    )

}

export default AppProvider