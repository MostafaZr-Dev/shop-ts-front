import IAddress from "@contracts/IAddress"
import ICart from "@contracts/ICart"
import ICoupon from "@contracts/ICoupon"
import IUser from "@contracts/IUser"

export type AppState = {
    user: IUser | null;
    cart: ICart[];
    coupon: ICoupon | null;
    deliveryAddress: IAddress | null;
    paymentMethod: string | null;
    paymentGateway: string | null;
}

const initState: AppState = {
    user: null,
    cart: [],
    coupon: null,
    deliveryAddress: null,
    paymentMethod: null,
    paymentGateway: null
}

export default initState