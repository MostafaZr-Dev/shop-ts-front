import Action from "@contracts/Action"
import { AppState } from "./initState"


const appReducer = (state: AppState, action: Action) => {
    let newState = state

    switch (action.type) {

        case 'INIT_STATE':
            newState = action.payload
            break;
        case 'SET_USER':
            newState = {
                ...state,
                user: action.payload.user,
            }
            break
        case 'EMPTY_USER_INFO':
            newState = {
                ...state,
                user: null,
                deliveryAddress: null,
                paymentGateway: null,
                paymentMethod: null,
                coupon: null
            }
            break;
        case 'ADD_TO_CART':
            const findProduct = state.cart.filter((item) => item.productID === action.payload.item.productID).pop()

            if (!findProduct) {
                newState = {
                    ...state,
                    cart: [
                        ...state.cart,
                        {
                            ...action.payload.item
                        }
                    ]
                }
            } else {
                const newCart = state.cart.map((item) => {
                    console.log({ object: item.quantity + action.payload.item.quantity })
                    if (item.productID === action.payload.item.productID) {
                        return {
                            ...item,
                            quantity: item.quantity + action.payload.item.quantity
                        }
                    }

                    return item
                })

                newState = {
                    ...state,
                    cart: newCart
                }
            }
            break
        case 'UPDATE_CART_ITEM_QUANTITY':

            const newCart = state.cart.map((item) => {
                if (item.productID === action.payload.productID) {
                    return {
                        ...item,
                        quantity: action.payload.quantity
                    }
                }

                return item
            })

            newState = {
                ...state,
                cart: newCart
            }

            break
        case 'DELETE_CART_ITEM':
            const newCartItems = state.cart.filter(item => item.productID !== action.payload.productID)

            newState = {
                ...state,
                cart: newCartItems
            }

            break

        case 'UPDATE_COUPON':
            newState = {
                ...state,
                coupon: {
                    ...action.payload
                }
            }
            break
        case 'ADD_ADDRESS':
            newState = {
                ...state,
                user: state.user ? {
                    ...state.user,
                    addresses: action.payload.addresses
                } : null
            }
            break
        case 'UPDATE_PAYMETN_METHOD':
            newState = {
                ...state,
                paymentMethod: action.payload.paymentMethod
            }
            break;
        case 'UPDATE_PAYMETN_GATEWAY':
            newState = {
                ...state,
                paymentGateway: action.payload.paymentGateway
            }
            break;
        case 'UPDATE_DELIVERY_ADDRESS':
            newState = {
                ...state,
                deliveryAddress: action.payload.deliveryAddress
            }
            break;
        case 'EMPTY_CART':
            newState = {
                ...state,
                cart: [],
                coupon: null,
                deliveryAddress: null,
                paymentMethod: null,
                paymentGateway: null
            }
            break;
        default:
            throw new Error(`${action.type} is not defined!`)
    }

    localStorage.setItem('state', JSON.stringify(newState))

    return newState
}

export default appReducer