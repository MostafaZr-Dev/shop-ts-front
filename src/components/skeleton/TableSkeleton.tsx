import React from 'react'
import Skeleton from 'react-loading-skeleton';

function TableSkeleton() {
    return (
        <Skeleton count={5}/> 
    )
}

export default TableSkeleton
