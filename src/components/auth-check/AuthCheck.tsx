import React, { useState, useEffect } from 'react'
import { useRouter } from "next/router";

import useAppState from 'state/useAppState'
import useGetAPI from '@hooks/useGetAPI'
import Loader from '@components/loader'

const isClient = typeof window !== 'undefined' ? true : false

function AuthCheck({ children }: React.PropsWithChildren<{}>) {
    const router = useRouter()

    const { state, dispatch } = useAppState()

    const [authCheckResponse, authCheckAPI] = useGetAPI({
        url:'/api/v1/auth/check',
        headers: {
            'Authorization': `Bearer ${isClient ? localStorage.getItem('token'): ''}`
        }
    })

    const { isLoading, success, data, error } = authCheckResponse

    useEffect(() => {
        authCheckAPI()
    }, [])

    useEffect(() => {
        if(success){
            dispatch({
                type:'SET_USER',
                payload: {
                    user: data.user
                }
            })
        }
    }, [success])

    useEffect(() => {
        if(error){
            dispatch({
                type:'EMPTY_USER_INFO',
                payload: {
                    user: null
                }
            })
            router.push("/auth/login")
        }
    }, [error])

    return (
        <>
            {isLoading && <Loader />}
            {!isLoading && state.user && <> { children } </>}
        </>
    )
}

export default AuthCheck
