import React from 'react'
import Link from 'next/link'

function EmptyCart() {
    return (
        <div className='row'>
            <div className='col-md-12'>
                <div className="alert alert-warning alert-dismissible fade show text-center" role="alert">
                    <p>
                    سبد خرید شما خالی است
                    </p>
                    <p>
                    می‌توانید برای مشاهده محصولات بیشتر به صفحه محصولات بروید
                    </p>
                    <p>
                        <Link href='/products'>
                            <a className='btn btn-solid-dark'>
                                محصولات
                            </a>
                        </Link>
                    </p>
                </div>
            </div>
        </div>
    )
}

export default EmptyCart
