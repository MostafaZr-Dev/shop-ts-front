import React from 'react'

import ICart from '@contracts/ICart'
import CartItem from './CartItem'

type CartListProps = {
    items: ICart[];
    onChangeItemQuantity: (itemID:string, quantity: number) => void
    onDeleteCartItem: (itemID:string) => void
}

function CartList({ items, onChangeItemQuantity, onDeleteCartItem }:CartListProps) {
    const renderCartItems = items.map((item) => (
        <CartItem 
            key={item.productID} 
            item={item}
            onChangeItemQuantity={(e:React.ChangeEvent<HTMLInputElement>) => {
                const quantity = parseInt(e.target.value)
                onChangeItemQuantity(item.productID, quantity)
            }}
            onDeleteCartItem = {(e:React.MouseEvent<HTMLAnchorElement>) => {
                e.preventDefault()
                onDeleteCartItem(item.productID)
            }}
        />
    ))

    return (
        <div className="row">
            <div className="col-md-12">
                <div className="table-responsive">
                    <table className="table vl-custom-table">
                        <thead>
                            <tr>
                                <th>نام محصول</th>
                                <th>قیمت</th>
                                <th>قیمت با تخفیف</th>
                                <th>تعداد</th>
                                <th>مجموع</th>
                            </tr>
                        </thead>
                        <tbody>
                            {renderCartItems}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default CartList
