import React from 'react'
import Image from 'next/image'

import ICart from '@contracts/ICart'
import CurrencyService from '@services/currencyService'
import products from '@components/products'

type CartItemProps = {
    item: ICart;
    onChangeItemQuantity: (e:React.ChangeEvent<HTMLInputElement>) => void
    onDeleteCartItem: (e:React.MouseEvent<HTMLAnchorElement>) => void
}

function CartItem({ item, onChangeItemQuantity, onDeleteCartItem }:CartItemProps) {
    const finalPrice = (price:number, discountedPrice:number, quantity: number) => {
        return discountedPrice ? discountedPrice * quantity : price * quantity
    }

    return (
        <tr>
            <td>
                <div className="d-flex align-items-center">
                    <a href="#" className="mr-4">
                        <Image 
                            className="rounded" 
                            src={item.productThumbnail} 
                            width={100} 
                            height={90} 
                            alt=""
                        />
                    </a>
                    <a href="#" className="text-dark">{item.productTitle}</a>
                </div>
            </td>
            <td>
                <strong>{CurrencyService.formatIRR(item.price)}</strong>
            </td>
            <td>
                <strong>{CurrencyService.formatIRR(item.discountedPrice)}</strong>
            </td>
            <td>
                <input 
                    type="number" 
                    defaultValue={item.quantity} 
                    min={1} 
                    className="form-control w-50"
                    onChange={onChangeItemQuantity}
                />
            </td>
            <td>
                <strong>{CurrencyService.formatIRR(finalPrice(item.price, item.discountedPrice, item.quantity))}</strong>
            </td>
            <td>
                <a onClick={onDeleteCartItem} className="text-decoration-none h5"><i className="vl-cross-circle"></i></a>
            </td>
        </tr>
    )
}

export default CartItem
