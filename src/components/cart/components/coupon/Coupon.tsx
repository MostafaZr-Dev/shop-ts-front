import React, { useState, useCallback } from 'react'

type CouponProps = {
    onApplyCoupon: (code: string) => void
}

function Coupon({ onApplyCoupon } : CouponProps) {

    const [code, setCode] = useState<string>('')

    const handleApplyCoupon = useCallback(
        () => {
            onApplyCoupon(code)
        },
        [code, onApplyCoupon],
    )

    const handleChangeCode = useCallback(
        (e:React.ChangeEvent<HTMLInputElement>) => {
            setCode(e.target.value)
        },
        [],
    )

    return (
        <div className="col-md-7">
            <h6>تخفیف کوپن</h6>
            <p className="text-muted">لطفا کد کوپن خود را در صورت لزوم وارد کنید</p>

            <form className="d-md-flex mt-lg-4 mt-3 mb-4">
                <input 
                    type="text" 
                    className="form-control mr-2 mb-2" 
                    placeholder="کد کوپن"
                    onChange={handleChangeCode} 
                />
                <button 
                    type="button" 
                    className="btn btn-pill btn-solid-dark mr-2  mb-2"
                    onClick={handleApplyCoupon}
                >
                    اعمال کوپن
                </button>
                {/* <button type="submit" className="btn btn-pill btn-outline  mb-2">بروز سبد</button> */}
            </form>
        </div>
    )
}

export default Coupon
