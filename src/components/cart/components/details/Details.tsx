import React from 'react'
import Link from 'next/link'

import ICoupon from '@contracts/ICoupon'
import ICart from '@contracts/ICart'
import CurrencyService from '@services/currencyService'
import { amountWithDiscount, calculateDiscountAmount } from '@services/discountService'
import LangService from '@services/langService'

type DetailsProps = {
    coupon: ICoupon | null;
    cart: ICart[];
}

function Details({ coupon, cart }: DetailsProps) {

    const totalCart = cart.reduce((total, item) => {
        const price = item.discountedPrice ? item.discountedPrice : item.price;
        return total + (price * item.quantity)
    }, 0)

    return (
        <div className="col-md-5">
            <div className="card p-4">
                <h6>مجموع سبد</h6>
                <hr />
                <div className="row mb-1">
                    <div className="col-8 font-weight-normal">مجموع کل</div>
                    <div className="col-4">{CurrencyService.formatIRR(totalCart)}</div>
                </div>
                {coupon && (
                    <div className="row mb-1">
                        <div className="col-8 font-weight-normal">تخفیف ({LangService.toPersianNumber(coupon.percent)}%)</div>
                        <div className="col-4"> {CurrencyService.formatIRR(calculateDiscountAmount(totalCart, coupon.percent))}-</div>
                    </div>
                )}
                <div className="row my-4">
                    <div className="col-8">مجموع خرید</div>
                    <div className="col-4"><strong>{CurrencyService.formatIRR(amountWithDiscount(totalCart, coupon ? coupon.percent : 0))}</strong></div>
                </div>
                <Link href='/checkout'>
                    <a className="btn btn-pill btn-block btn-theme">
                        بررسی
                    </a>
                </Link>
            </div>
        </div>
    )
}

export default Details
