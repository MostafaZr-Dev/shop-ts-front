import React, { useEffect, useCallback } from 'react'
import CartList from './components/cart-list'

import useAppState from 'state/useAppState'
import Header from './components/header'
import Coupon from './components/coupon'
import Details from './components/details'
import EmptyCart from './components/empty-cart'
import usePostAPI from '@hooks/usePostAPI'
import Alert from '@components/alert'

function Cart() {
    const { state, dispatch } = useAppState()

    const [couponValidationResponse, validateCouponAPI] = usePostAPI({
        url: '/api/v1/coupons/validation'
    })

    const {
        isLoading: couponValidationLoading, 
        success: couponValid, 
        error: couponInvalid,
        data: couponValidationData
    } = couponValidationResponse

    useEffect(() => {
        if(couponValid){
            dispatch({
                type: "UPDATE_COUPON",
                payload: {
                    code: couponValidationData.coupon.code,
                    percent: couponValidationData.coupon.percent
                }
            })
        }
    }, [couponValid, couponValidationData, dispatch])

    const updateItemQuantity = useCallback(
        (productID: string, quantity: number) => {
            dispatch({
                type:'UPDATE_CART_ITEM_QUANTITY',
                payload: {
                    productID,
                    quantity
                }
            })
        },
        [dispatch],
    )

    const deleteCartItem = useCallback(
        (productID: string) => {
            dispatch({
                type: 'DELETE_CART_ITEM',
                payload: {
                    productID
                }
            })
        },
        [dispatch],
    )

    const applyCoupon = (code: string) => {
        if(code === '' || code.trim() === ''){
            return
        }

        console.log(code)

        validateCouponAPI({
            couponCode: code
        })
    }

    return (
        <>
            <Header />
            <section className="section-gap">
                <div className="container">
                    {state.cart.length === 0 && (
                        <EmptyCart />
                    )}
                    {state.cart.length > 0 && (
                        <>
                            <CartList 
                                items={state.cart} 
                                onChangeItemQuantity={updateItemQuantity}
                                onDeleteCartItem={deleteCartItem}
                            />
                            <div className="row">
                                <div className='col-md-12'>
                                    <Alert show={couponValid} message='کد تخفیف با موفقیت اعمال شد'/>
                                    <Alert show={couponInvalid} type="danger" message={couponValidationData}/>
                                </div>
                                <Coupon onApplyCoupon={applyCoupon}/>
                                <Details coupon={state.coupon} cart={state.cart}/>
                            </div>
                        </>
                    )}
                </div>
            </section>
        </>
    )
}

export default Cart
