import React from 'react'

type AlertProps = {
    show: boolean;
    type?: 'success' | 'danger' | 'warning' | 'primary' | 'secondary' | undefined;
    message: React.ReactNode | string;
}

function Alert({ show, type, message }: AlertProps) {
    const alertClass:string = type ? `alert-${type}` : `alert-success`
    
    if(!show){
        return null
    }

    return (
        <div className={`alert ${alertClass} alert-dismissible fade show`} role="alert">
            {message}
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    )
}

export default Alert
