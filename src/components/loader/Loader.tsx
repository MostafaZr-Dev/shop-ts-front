import React from 'react'

function Loader() {
    return (
        <div className="spinner-container d-flex justify-content-center align-items-center">
            <div className="spinner-cmp"></div>
        </div>
    )
}

export default Loader
