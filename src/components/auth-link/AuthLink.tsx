import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

type AuthLinkProps = {
    title: string;
    description: string;
}

function AuthLink({ title, description }:AuthLinkProps) {
    return (
        <div className="card border-0 mb-md-0 mb-3 box-hover">
            <Image 
                className="card-img-top" 
                src="/static/assets/img/cards/1a.jpg" 
                width={610} 
                height={190} 
                alt="auth-image" 
            />
            <div className="card-body py-4 text-center">
                <h5 className="mb-4">{title}</h5>
                <div className="mb-4">
                    <p>{description}</p>
                </div>
            </div>
            <div className="card-footer">
                <div className="d-flex align-items-center justify-content-center">
                    <Link href='/auth/login'>
                        <a className='btn btn-sm btn-pill btn-theme mr-4'>ورود به سایت</a>
                    </Link>
                    <Link href='/auth/register'>
                        <a className='btn btn-sm btn-pill btn-solid-dark'>ثبت نام</a>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default AuthLink
