import React, { useEffect,useState } from 'react'

import Header from './components/header'
import Content from './components/content'
import usePostAPI from '@hooks/usePostAPI'
import Alert from '@components/alert'
import useAppState from 'state/useAppState'

const getPaymentParams = () => {
    const url = new URL(window.location.href)

    return {
        authority: url.searchParams.get('Authority'),
        status:  url.searchParams.get('Status'),
        reserve:  url.pathname.split('/')[3],
    }
}

const verifyStatus = {
    PENDING: 0,
    SUCCESS: 1,
    FAILED: 2
}

function PaymentVerify() {

    const [verifResult, setVerifResult] = useState<number>(verifyStatus.PENDING)

    const { dispatch } = useAppState()

    const [verifyPaymentResponse, verifyPaymentAPI] = usePostAPI({
        url: '/api/v1/purchase/verification',
        configs: {
            headers: {
                'Authorization': `Bearer ${!(typeof window === "undefined") ? localStorage.getItem('token') : ''}`
            }
        }
    })

    const {isLoading, success, error, data} = verifyPaymentResponse

    useEffect(() => {
        const { reserve, authority, status } = getPaymentParams()
        
        verifyPaymentAPI<{success: boolean, refID?:string}, object>({
            reserve,
            authority: authority,
            status: status 
        })
    }, [])


    useEffect(() => {
        if(success){

            dispatch({
                type:"EMPTY_CART",
                payload: null
            })

            if(data.success){
                setVerifResult(verifyStatus.SUCCESS)
            }else{
                setVerifResult(verifyStatus.FAILED)
            }

        }
    }, [success])

    useEffect(() => {
        if(error){
            dispatch({
                type:"EMPTY_CART",
                payload: null
            })
            
            setVerifResult(verifyStatus.FAILED)
        }
    }, [error])

    return (
        <>
            <Header title='نتیجه پرداخت'/>
            <Content>
                <div className="col-md-12 order-md-1">
                    <div className="card p-4">
                        <h5 className='mb-5'>پرداخت سفارش</h5>
                        {verifResult === verifyStatus.PENDING && <p>در حال بررسی تراکنش...</p>}
                        {verifResult === verifyStatus.SUCCESS && <Alert show={true} type='success' message={
                            <p>
                                پرداخت موفق
                                <br />
                                شماره پیگیری : {data?.refID}
                            </p>
                        }/>}
                        {verifResult === verifyStatus.FAILED && <Alert show={true} type='danger' message={
                            <p>
                                پرداخت ناموفق
                            </p>
                        }/>}
                    </div>
                </div>
            </Content>
        </>
    )
}

export default PaymentVerify
