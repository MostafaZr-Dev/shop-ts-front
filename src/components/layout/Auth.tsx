import React, { Children } from 'react'
import Head from 'next/head'

type AuthLayoutProps = {
    title: string;
    children: React.ReactNode;
}

function AuthLayout({ title, children }:AuthLayoutProps) {
    return (
        <>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
                <meta name="description" content="" />
                <meta name="author" content="Mosaddek" />
                <title>{title}</title>
            </Head>
            {children}
        </>
    )
}

export default AuthLayout
