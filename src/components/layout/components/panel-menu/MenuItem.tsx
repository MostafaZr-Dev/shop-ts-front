import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'

type MenuItemProps = {
    href:string;
    title: string;
    icon: React.ReactNode;
    onClick?: (e: React.MouseEvent) => void
}

function MenuItem({href, title, icon, onClick} : MenuItemProps) {
    const router = useRouter()

    return (
        <li>
            <Link href={href} passHref>
                <a 
                    className={router.pathname === href ? 'active-link' : ''} 
                    onClick={onClick}
                >
                    {icon}
                    {title}
                </a>
            </Link>
        </li>
    )
}

export default MenuItem
