import React from 'react'
import Image from 'next/image'
import { useRouter } from 'next/router'

import MenuItem from './MenuItem'
import useAppState from 'state/useAppState'

function PanelMenu() {
    const { state, dispatch } = useAppState()
    const router = useRouter()

    const logout  = (e: React.MouseEvent) => {
        e.preventDefault();

        localStorage.removeItem('token')

        dispatch({
            type:'EMPTY_USER_INFO',
            payload: null
        })

        router.push('/auth/login')
    }
    console.log(state.user)
    return (
        <div className='col-md-3'>
            <div className="card border-0 mb-md-0">
                <div className="card-body py-4">
                    <div className='text-center mb-4'>
                        <h4>پنل کاربری</h4>
                        <div className="d-flex flex-column justify-content-center align-items-center pt-4">
                            <Image 
                                className="avatar-sm rounded-circle mb-3" 
                                src="/static/assets/img/avatar1.jpg" 
                                width={150} 
                                height={150} 
                                alt="user-avatar" 
                            />
                            <div>
                                <strong className="">{state.user?.fullName}</strong>
                            </div>
                        </div>
                    </div>
                    <ul className="custom-list custom-list-border">
                        <MenuItem href='/panel' title='داشبورد' icon={<i className="fa fa-desktop fa-lg pr-3" />} />  
                        <MenuItem href='/panel/orders' title='سفارش ها' icon={<i className="fa fa-shopping-bag fa-lg pr-3" />} />  
                        <MenuItem href='/panel/addresses' title='آدرس ها' icon={<i className="fa fa-address-card fa-lg pr-3" />} />  
                        <MenuItem 
                            href='#' 
                            title='خروج' 
                            icon={<i className="fa fa-sign-out fa-lg pr-3" aria-hidden="true" />} 
                            onClick={logout}
                        />  
                    </ul>
                </div>
            </div>    
        </div>
    )
}

export default PanelMenu
