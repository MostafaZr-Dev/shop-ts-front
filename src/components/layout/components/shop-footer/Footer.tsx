import React from 'react'
import Image from 'next/image'

function Footer() {
    return (
        <>
            {/*footer start*/}
            <footer className="app-footer bg-dark pb-0 border-0 text-md-left text-center">
                <div className="container">
                <div className="row align-items-center mb-md-5 mb-3">
                    <div className="col-md-4">
                    <Image
                        className="pr-3 mb-md-0 mb-4"
                        src="/static/assets/img/logo-color.png"
                        // srcSet="static/assets/img/logo-color@2x.png 2x"
                        alt=''
                        layout='fill'
                    />
                    </div>
                    <div className="col-md-8">
                    <span className="font-lora h5 font-weight-normal">
                        - یک بسته ساز قدرتمند خلاق برای بوت استرپ 4
                    </span>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4 mb-md-0 mb-4">
                    <p className="text-muted">
                        یک مجموعه بزرگ و قدرتمند از بسته بندی جزء مدرن برای ساختن وب سایت
                        بهتر برای پروژه بعدی شما
                    </p>
                    </div>
                    <div className="col-md-2 mb-md-0 mb-4">
                    <h6 className="mb-4">حرکت کن</h6>
                    <ul className="footer-link">
                        <li className="d-block">
                        <a href="#">بررسی</a>
                        </li>
                        <li className="d-block">
                        <a href="#">صفحات لندینگ</a>
                        </li>
                        <li className="d-block">
                        <a href="#">صفحات اپ</a>
                        </li>
                        <li className="d-block">
                        <a href="#">صفحات داخلی</a>
                        </li>
                    </ul>
                    </div>
                    <div className="col-md-2 mb-md-0 mb-4">
                    <h6 className="mb-4">پلتفرم</h6>
                    <ul className="footer-link">
                        <li className="d-block">
                        <a href="#">مک و iOS</a>
                        </li>
                        <li className="d-block">
                        <a href="#">آندروید و کروم</a>
                        </li>
                        <li className="d-block">
                        <a href="#">ویندوز</a>
                        </li>
                        <li className="d-block">
                        <a href="#">لینوکس</a>
                        </li>
                    </ul>
                    </div>
                    <div className="col-md-2 mb-md-0 mb-4">
                    <h6 className="mb-4">جامعه</h6>
                    <ul className="footer-link">
                        <li className="d-block">
                        <a href="#">پایگاه دانش</a>
                        </li>
                        <li className="d-block">
                        <a href="#">یک کارشناس استخدام کنید</a>
                        </li>
                        <li className="d-block">
                        <a href="#">گفت و گو</a>
                        </li>
                        <li className="d-block">
                        <a href="#">تماس</a>
                        </li>
                    </ul>
                    </div>
                    <div className="col-md-2 mb-md-0 mb-4">
                    <h6 className="mb-4">شرکت</h6>
                    <ul className="footer-link">
                        <li className="d-block">
                        <a href="#">درباره شرکت</a>
                        </li>
                        <li className="d-block">
                        <a href="#">داستان</a>
                        </li>
                        <li className="d-block">
                        <a href="#">تیم</a>
                        </li>
                        <li className="d-block">
                        <a href="#">سرمایه گذاری</a>
                        </li>
                    </ul>
                    </div>
                </div>
                </div>
                <div className="app-secondary-footer mt-md-5 mt-3">
                <div className="container">
                    <div className="row">
                    <div className="col">
                        <span className="copyright">© 2019 کلاب. تمام حقوق محفوظ است.</span>
                    </div>
                    </div>
                </div>
                </div>
            </footer>
            {/*footer end*/}
        </>

    )
}

export default Footer
