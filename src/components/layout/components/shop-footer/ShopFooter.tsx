import React from 'react'

import Footer from './Footer'
import Scripts from './Scripts'

function ShopFooter() {
    return (
        <>
            <Footer />
            <Scripts />
        </>
    )
}

export default ShopFooter
