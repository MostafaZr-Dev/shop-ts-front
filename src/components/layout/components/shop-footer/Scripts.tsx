import React from 'react'
import Script from 'next/script'

function Scripts() {
    return (
        <>
            {/* <!--basic scripts--> */}
            <Script src="/static/assets/vendor/jquery/jquery.min.js"></Script>
            <Script src="/static/assets/vendor/popper.min.js"></Script>
            <Script src="/static/assets/vendor/bootstrap/js/bootstrap.min.js"></Script>
            <Script src="/static/assets/vendor/vl-nav/js/vl-menu.js"></Script>
            <Script src="/static/assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></Script>
            <Script src="/static/assets/vendor/owl/owl.carousel.min.js"></Script>
            <Script src="/static/assets/vendor/jquery.animateNumber.min.js"></Script>
            <Script src="/static/assets/vendor/jquery.countdown.min.js"></Script>
            <Script src="/static/assets/vendor/typist.js"></Script>
            <Script src="/static/assets/vendor/jquery.isotope.js"></Script>
            <Script src="/static/assets/vendor/imagesloaded.js"></Script>
            <Script src="/static/assets/vendor/visible.js"></Script>
            <Script src="/static/assets/vendor/wow.min.js"></Script>

            {/* <!--basic scripts initialization--> */}
            <Script src="/static/assets/js/scripts.js"></Script>
        </>
    )
}

export default Scripts
