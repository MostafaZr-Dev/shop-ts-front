import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

import useAppState from 'state/useAppState'

function PreHeader() {
    const { state } = useAppState()
    return (
        <>
        {/*pre header start*/}
            <section className="py-2 bg-gray">
                <div className="container">
                <div className="row">
                    <div className="col-md-7">
                    <small className="font-size-14">
                        هر سوالی دارید؟ با ما تماس بگیرید: 1-222-333-4445
                    </small>
                    </div>
                    <div className="col-md-5 text-md-right">
                    <ul className="list-inline m-0 d-inline mr-2">
                        <li className="list-inline-item font-size-14">
                            <Link href='/auth/login'>
                                <a className="text-dark">
                                    ورود
                                </a>
                            </Link>
                        </li>
                        <li className="list-inline-item font-size-14 ml-2">
                            <Link href='/auth/register'>
                                <a className="text-dark">
                                    ایجاد حساب کاربری
                                </a>
                            </Link>
                        </li>
                    </ul>
                    <Link href='/cart'>
                        <a className="text-decoration-none text-dark ml-2">
                            <i className="vl-cart1" />
                            <span className='ft-tag'>{state.cart.length}</span>
                        </a>
                    </Link>
                    {state.user && (
                        <Link href='/panel'>
                            <a className="text-decoration-none text-dark ml-2">
                                <i className="fa fa-user-circle fa-lg" />
                            </a>
                        </Link>
                    )}
                    </div>
                </div>
                </div>
            </section>
            {/*pre header end*/}
        </>
    )
}

export default PreHeader
