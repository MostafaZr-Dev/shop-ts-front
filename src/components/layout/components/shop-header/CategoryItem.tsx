import React from 'react'
import Link from 'next/link'

import ICategory from '@contracts/ICategory'

type CategoryItemProps = {
    subCategories: ICategory[]
}

function CategoryItem({ subCategories }: CategoryItemProps) {

    const renderSubCategory = subCategories.map((category) => (
        <li key={category.id}>
            <Link href={{
                pathname: `/category/[slug]`,
                query: {
                    slug: category.slug
                }
            }}>
                <a>{category.title}</a>
            </Link>
        </li>
    ))

    return (
        <div className="col5">
            <ol>
                {renderSubCategory}
            </ol>
        </div>
    )
}

export default CategoryItem
