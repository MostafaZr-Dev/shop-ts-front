import React, { useState, useEffect } from 'react'
import { chunk } from 'lodash'

import PreHeader from './PreHeader'
import TopHeader from './TopHeader'
import useGetAPI from '@hooks/useGetAPI'
import ICategory from '@contracts/ICategory'

function ShopHeader() {
    const [categories, setCategories] = useState<ICategory[]>([])
    const [getCategoriesResponse, getCategoriesFromAPI] = useGetAPI({
        url:'/api/v1/categories',
        headers: {}
    })

    const { success, error, data } = getCategoriesResponse

    useEffect(() => {
        getCategoriesFromAPI<{categories: ICategory[]}>()
    }, [])

    useEffect(() => {
        if(success){
            setCategories(data.categories)
        }
    }, [success])

    return (
        <>
            <PreHeader />
            <TopHeader categories={chunk(categories, 8)}/>
        </>
    )
}

export default ShopHeader
