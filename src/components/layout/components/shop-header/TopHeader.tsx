import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

import ICategory from '@contracts/ICategory'
import CategoryItem from './CategoryItem'

type TopHeaderProps = {
    categories: ICategory[][]
} 

function TopHeader({ categories }:TopHeaderProps ) {

    const renderCategories = categories.map((chunkedCategories, index) => (
        <CategoryItem key={`cat-item-${index}`} subCategories={chunkedCategories} />
    ))

    return (
        <>
            {/*header start*/}
            <header className="app-header">
                <div className="container">
                <div className="row">
                    <div className="col-12">
                    {/*<div class="branding-wrap">*/}
                    {/*brand start*/}
                    <div className="navbar-brand float-left">
                        <a href="index-2.html">
                        <Image
                            className="logo-light"
                            src="/static/assets/img/logo.png"
                            // srcSet="static/assets/img/logo@2x.png 2x"
                            alt="CLab"
                            width={76}
                            height={28}
                        />
                        <Image
                            className="logo-dark"
                            src="/static/assets/img/logo-dark.png"
                            // srcSet="static/assets/img/logo-dark@2x.png 2x"
                            width={76}
                            height={28}
                            alt="CLab"
                        />
                        </a>
                    </div>
                    {/*brand end*/}
                    {/*start responsive nav toggle button*/}
                    <div className="nav-btn hamburger hamburger--slider js-hamburger ">
                        <div className="hamburger-box">
                        <div className="hamburger-inner" />
                        </div>
                    </div>
                    {/*end responsive nav toggle button*/}
                    {/*</div>*/}
                    {/*top mega menu start*/}
                    <nav id="vl-menu">
                        {/*extra links start*/}
                        <div className="float-right nav-extra-link">
                            <Link href='/products'>
                                <a className="btn btn-sm btn-pill btn-theme mt-3">
                                    محصولات
                                </a>
                            </Link>
                        </div>
                        {/*extra links end*/}
                        {/*start nav*/}
                        <ul className="vlmenu light-sub-menu slide-effect float-right">
                        <li>
                            <Link href="/">
                                <a>
                                خانه
                                </a>
                            </Link>
                        </li>
                        <li>
                            <a href="#">
                            دسته بندی ها <i className="fa fa-angle-down" />
                            </a>
                            {/*mega menu start*/}
                            <div className="mega-menu full-nav  ">
                                {renderCategories}
                            </div>
                            {/*mega menu end*/}
                        </li>
                        <li>
                            <Link href='/about-us'>
                                <a>
                                {" "}
                                درباره ما
                                </a>
                            </Link>
                        </li>
                        <li>
                            <Link href='/support'>
                                <a>
                                {" "}
                                پشتیبانی
                                </a>
                            </Link>
                        </li>
                        </ul>
                        {/*end nav*/}
                    </nav>
                    {/*top mega menu end*/}
                    </div>
                </div>
                </div>
            </header>
            {/*header end*/}
    </>

    )
}

export default TopHeader
