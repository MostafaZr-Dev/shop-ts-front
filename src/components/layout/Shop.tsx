import React, { useState, useEffect } from 'react'
import Head from 'next/head'

import Header from './components/shop-header'
import Footer from './components/shop-footer'
import useAppState from 'state/useAppState'
import useGetAPI from '@hooks/useGetAPI'

const isClient = typeof window !== 'undefined' ? true : false

type ShopLayoutProps = {
    children: React.ReactNode;
    title: string;
}

function ShopLayout({title, children}: ShopLayoutProps) {
    const { dispatch } = useAppState()

    const [authCheckResponse, authCheckAPI] = useGetAPI({
        url:'/api/v1/auth/check',
        headers: {
            'Authorization': `Bearer ${isClient ? localStorage.getItem('token'): ''}`
        }
    })

    const { success, data, error } = authCheckResponse

    useEffect(() => {
        const initState = () => {
            const currentState = localStorage.getItem('state')

            if(currentState){
                dispatch({
                    type: 'INIT_STATE',
                    payload: JSON.parse(currentState)
                })
            }
        }

        initState()
    }, [])

    useEffect(() => {
        authCheckAPI()
    }, [])

    useEffect(() => {
        if(success){
            dispatch({
                type:'SET_USER',
                payload: {
                    user: data.user
                }
            })
        }
    }, [success])

    useEffect(() => {
        if(error){
            dispatch({
                type:'SET_USER',
                payload: {
                    user: null
                }
            })
        }
    }, [error])

    return (
        <>
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0, maximum-scale=1.0"
                />
                <meta name="description" content='' />
                <meta name="author" content="Mosaddek" />
                <title>{title}</title>
            </Head>
            <Header />
            {children}
            <Footer />
        </>
    )
}

export default ShopLayout
