import React from 'react'
import Head from 'next/head'
import ShopHeader from './components/shop-header'
import PanelMenu from './components/panel-menu'
import AuthCheck from '@components/auth-check'

type PanelLayoutProps = {
    title: string;
    children:React.ReactNode;
}

function PanelLayout({ title, children }: PanelLayoutProps) {
    return (
        <AuthCheck>
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0, maximum-scale=1.0"
                />
                <meta name="description" content='' />
                <meta name="author" content="Mosaddek" />
                <title>{title}</title>
            </Head>
            <ShopHeader />
            <div className="component-section bg-gray mt-3">
                <div className="container">
                    <div className="row justify-content-center">
                        <PanelMenu />
                        <div className="col-md-9">
                            <div className="card">
                                <div className="card-body">
                                    {children}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </AuthCheck>
    )
}

export default PanelLayout
