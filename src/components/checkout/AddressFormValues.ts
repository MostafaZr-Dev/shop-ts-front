export default interface AddressFormValues {
    title: string;
    fullName: string;
    mobile: string;
    state: string;
    city: string;
    zipCode: string;
    address: string;
}