import React, { useEffect, useState } from 'react'

import Header from './components/header'
import Content from './components/content'
import CartDetails from './components/cart-details'
import Address from './components/address'
import Payment from './components/payment'
import useAppState from 'state/useAppState'
import AuthLink from '@components/auth-link'
import AddressFormValues from './AddressFormValues'
import usePostAPI from '@hooks/usePostAPI'
import useGetAPI from '@hooks/useGetAPI'
import IMethodsList from './IMethodsList'

function Checkout() {
    const [methodsList, setmethodsList] = useState<IMethodsList[]>([])

    const { state, dispatch } = useAppState()

    const [newAddressResponse, newAddressAPI] = usePostAPI({
        url: `/api/v1/users/${state.user?.id}/addresses`,
        configs: {
            headers: {
                'Authorization': `Bearer ${!(typeof window === "undefined") ? localStorage.getItem('token') : ''}`
            }
        }
    })

    const [methodsListResponse, getMethodsListAPI] = useGetAPI({
        url: '/api/v1/payments/methods',
        headers: {}
    })

    const [payOrderResponse, payOrderAPI] = usePostAPI({
        url: `/api/v1/purchase`,
        configs: {
            headers: {
                'Authorization': `Bearer ${!(typeof window === "undefined") ? localStorage.getItem('token') : ''}`
            }
        }
    })

    const {
        isLoading: methodsListLoading,
        success: methodsListSuccess,
        data: methodsListData,
        error: methodsListError
    } = methodsListResponse

    const {
        isLoading: newAddressLoading,
        success: newAddressSuccess,
        error: newAddressError,
        data: newAddressData
    } = newAddressResponse

    const {
        isLoading: payOrderLoading,
        success: payOrderSuccess,
        error: payOrderError,
        data: payOrderData
    } = payOrderResponse

    useEffect(() => {
        getMethodsListAPI<{success: boolean, methods: IMethodsList[]}>()
    }, [])

    useEffect(() => {
        if(methodsListSuccess){
            setmethodsList(methodsListData.methods)
        }
    }, [methodsListSuccess])

    useEffect(() => {
        if(newAddressSuccess){
            dispatch({
                type: `ADD_ADDRESS`,
                payload :{
                    addresses: newAddressData.addresses
                }
            })
        }
    }, [newAddressSuccess])


    useEffect(() => {
        if(payOrderSuccess){
            
            if(payOrderData.url){
                window.location.replace(payOrderData.url)
            }
            
        }
    }, [payOrderSuccess])

    const saveAddress = (values: AddressFormValues) => {
        console.log({values})

        newAddressAPI(values)
    }

    const payOrder = () => {
        
        payOrderAPI({
            cart: state.cart,
            coupon: state.coupon,
            deliveryAddress: state.deliveryAddress,
            method: state.paymentMethod,
            gateway: state.paymentGateway
        })

    }

    const {user} = state
    
    return (
        <>
            <Header title='بررسی سفارش'/>
            <Content>
                <CartDetails items={state.cart} coupon={state.coupon}/>
                <div className="col-md-8 order-md-1">
                    {!user && (
                        <AuthLink 
                            title='ورود و ثبت نام' 
                            description='برای ثبت سفارش باید وارد سایت شوید یا ثبت نام کنید'
                        />
                    )}
                    {user && (
                        <>
                            <div className="row mb-2">
                                <div className="col-md-12">
                                    <h5 className="mb-4">انتخاب آدرس</h5>
                                    <Address addresses={user.addresses} onNewAddress={saveAddress} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <h5 className="mb-4">روش پرداخت</h5>
                                    <Payment methods={methodsList}/>
                                    <button 
                                        className="btn btn-primary btn-lg btn-block mt-3" 
                                        type="button"
                                        onClick={payOrder}
                                    >
                                        {payOrderLoading && <span>در حال پردازش...</span>}
                                        {!payOrderLoading && <span>پرداخت</span>}
                                    </button>
                                </div>
                            </div>
                        </>
                    )}
                </div>    
            </Content>
        </>
    )
}

export default Checkout
