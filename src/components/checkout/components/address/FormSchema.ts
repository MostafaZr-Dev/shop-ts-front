import * as yup from 'yup'

const schema = yup.object().shape({
    title: yup.string().required('فیلد الزامی است!'),
    fullName: yup.string().required('فیلد الزامی است!'),
    mobile: yup.string().required('فیلد الزامی است!'),
    state: yup.string().required('فیلد الزامی است!'),
    city: yup.string().required('فیلد الزامی است!'),
    zipCode: yup.string().required('فیلد الزامی است!')
        .test('check_length', 'کدپستی 10 رقم!', value => value?.length === 10)
    ,
    address: yup.string().required('فیلد الزامی است!')
})

export default schema