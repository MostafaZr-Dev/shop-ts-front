import React from 'react'

import IAddress from '@contracts/IAddress'

type AddressItemProps = {
    address: IAddress;
    id: string;
    onChangeAddress: (e:React.ChangeEvent<HTMLInputElement>) => void
}

function AddressItem({ address, id, onChangeAddress }: AddressItemProps) {
    return (
            <a className="list-group-item border-0">
                <div className="d-flex align-items-center">
                    <div className="custom-control custom-radio custom-control-inline">
                        <input
                            type="radio"
                            className="custom-control-input"
                            name="deliveryAddress"
                            onChange={onChangeAddress}
                            id={id}
                        />
                        <label className="custom-control-label" htmlFor={id}>
                                {address.title}
                        </label>
                    </div>
                    <div>
                        <h6 className="mb-0">{address.title}</h6>
                        <span>{address.state}</span>
                        <span> - </span>
                        <span>{address.city}</span>
                        <span> - </span>
                        <span>{address.address}</span>
                    </div>
                </div>
            </a>           
    )
}

export default AddressItem
