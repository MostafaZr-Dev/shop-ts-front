import React, { useState, useCallback, useEffect } from 'react'
import { debounce } from 'lodash'

import IranStates from '@utils/iranstates.json'
import FormSchema from './FormSchema'
import AddressFormValues from '@components/checkout/AddressFormValues'

const iranStates: {[key:string]: string[]} = IranStates

const states: string[] = Object.keys(iranStates)

type FormErrors = {
    [key: string]: string
}

type FormProps = {
    onSubmit: (values: AddressFormValues) => void
}

function Form({ onSubmit } : FormProps) {
    const [selectedState, setSelectedState] = useState<string>(states[0])

    const [formValues, setFormValues] = useState<AddressFormValues>({
        fullName: '',
        title: '',
        mobile: '',
        city: '',
        address: '',
        state:states[0],
        zipCode:''
    })
    const [formErrors, setFormErrors] = useState<FormErrors | null>(null)


    useEffect(() => {
        const validateFormData = (values: object) => {
            FormSchema.validate(values ,{ abortEarly: false }).then(values => {
                setFormErrors(null)
            })
            .catch(error => {
                const errors = error.inner.reduce((formError: object,innerError: any) => {
                    return {
                        ...formError,
                        [innerError.path] : innerError.message
                    }
                }, {})
    
                setFormErrors(errors)
    
            })
        }

        validateFormData(formValues)

    }, [formValues])

    

    const handleChange = debounce(
        (e:React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLSelectElement>) => {
            if(e.target.name === 'state'){
                setSelectedState(e.target.value)
            }
            
            setFormValues(prevValues => ({
                ...prevValues,
                [e.target.name] : e.target.value,
                ...(e.target.name === 'state' && {
                    city: ''
                })
            }))
    } , 500)

    const handleSubmit = useCallback(
        () => {
            if(formErrors){
                return
            }
    
    
            onSubmit(formValues)
        },
        [formErrors, formValues, onSubmit],
    )

    const cities: string[] = iranStates[selectedState]
    
    return (
        <form className="needs-validation" noValidate>
                <div className="mb-3">
                    <label htmlFor="title">عنوان آدرس</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title" 
                        name="title"
                        onChange={handleChange}
                    />
                    <div className="invalid-feedback d-block">
                            {formErrors && formErrors.title && (
                                <span>{formErrors.title}</span>
                            )}
                    </div>
                </div>
                <div className="mb-3">
                    <label htmlFor="fullName">نام کامل</label>
                    <input
                        type="text"
                        className="form-control"
                        id="fullName" 
                        name="fullName"
                        onChange={handleChange}
                    />
                    <div className="invalid-feedback d-block">
                            {formErrors && formErrors.fullName && (
                                <span>{formErrors.fullName}</span>
                            )}
                    </div>
                </div>
                <div className="mb-3">
                    <label htmlFor="mobile">شماره همراه</label>
                    <div className="input-group">
                        <input
                            name="mobile"
                            type="text"
                            className="form-control"
                            id="mobile"
                            placeholder="شماره موبایل"
                            onChange={handleChange}
                        />
                    </div>
                    <div className="invalid-feedback  d-block">
                            {formErrors && formErrors.mobile && (
                                <span>{formErrors.mobile}</span>
                            )}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-5 mb-3">
                        <label htmlFor="state">استان</label>
                        <select 
                            className="custom-select d-block w-100" 
                            id="state" 
                            name="state"
                            onChange={handleChange}
                        >
                            {states.map((state: string ,index) => (
                                <option key={index} value={state}>{state}</option>
                            ) )}
                        </select>
                        <div className="invalid-feedback d-block">
                            {formErrors && formErrors.state && (
                                <span>{formErrors.state}</span>
                            )}
                        </div>
                    </div>
                    <div className="col-md-4 mb-3">
                        <label htmlFor="city">شهر</label>
                        <select 
                            className="custom-select d-block w-100" 
                            id="city"
                            name="city" 
                            onChange={handleChange}
                        >
                            <option value=''>انتخاب شهر</option>
                            {cities.map((city, index) => (
                                <option key={`city-${index}`} value={city}>{city}</option>
                            ))}
                        </select>
                        <div className="invalid-feedback  d-block">
                            {formErrors && formErrors.city && (
                                <span>{formErrors.city}</span>
                            )}
                        </div>
                    </div>
                    <div className="col-md-3 mb-3">
                        <label htmlFor="zip">کد پستی</label>
                        <input
                        type="text"
                        name="zipCode"
                        className="form-control"
                        id="zip"
                        onChange={handleChange}
                        />
                        <div className="invalid-feedback  d-block">
                            {formErrors && formErrors.zipCode && (
                            <span>{formErrors.zipCode}</span>
                            )}
                        </div>
                    </div>
                </div>
                <div className="mb-3">
                    <label htmlFor="address">آدرس</label>
                    <input
                        type="text"
                        name="address"
                        className="form-control"
                        id="address"
                        placeholder="خیابان آذر پلاک 11"
                        onChange={handleChange}
                    />
                    <div className="invalid-feedback  d-block">
                        {formErrors && formErrors.address && (
                            <span>{formErrors.address}</span>
                        )}
                    </div>
                </div>
                <hr className="mb-4" />
                <button 
                    className="btn btn-primary btn-lg btn-block" 
                    type="button"
                    onClick={handleSubmit}
                >
                    افزودن
                </button>
        </form>
    )
}

export default Form
