import React from 'react'

import IAddress from '@contracts/IAddress'
import Form from './Form'
import AddressItem from './AddressItem'
import AddressFormValues from '@components/checkout/AddressFormValues'
import useAppState from 'state/useAppState'

type AdressProps = {
    addresses: IAddress[];
    onNewAddress: (values:AddressFormValues) => void
}

function Address({ addresses, onNewAddress }:AdressProps) {
    const { dispatch } = useAppState()

    const updateDeliveryAddress = (address: IAddress) => {
        dispatch({
            type: "UPDATE_DELIVERY_ADDRESS",
            payload: {
                deliveryAddress: address
            }
        })
    }

    const renderUserAddress = addresses.map((address, index) => (
        <AddressItem 
            key={`address-${index}`} 
            address={address} 
            id={`address-${index}`}
            onChangeAddress={(e) => {
                updateDeliveryAddress(address)
            }}
        />
    ))
    
    return (
        <div className="accordion accordion-style-1" id="accordion-1">
            <div className="card">
                <div className="card-header">
                    <h6>
                        <a
                        data-toggle="collapse"
                        data-target="#collapse-1-1"
                        aria-expanded="false"
                        >
                        آدرس های شما
                        </a>
                    </h6>
                </div>
                <div
                id="collapse-1-1"
                className="collapse show"
                data-parent="#accordion-1"
                style={{}}
                >
                    <div className="card-body">
                        <div className="list-group list-group-gap">
                            {addresses.length === 0 && (
                                <h6 className="alert alert-primary alert-dismissible fade show">آدرسی ثبت نکردید</h6>
                            )}
                            {addresses.length > 0 && <> {renderUserAddress} </>}
                        </div>
                    </div>
                </div>
            </div>
            <div className="card">
                <div className="card-header">
                <h6>
                    <a
                    className="collapsed"
                    data-toggle="collapse"
                    data-target="#collapse-1-2"
                    >
                    افزودن آدرس جدید
                    </a>
                </h6>
                </div>
                <div id="collapse-1-2" className="collapse" data-parent="#accordion-1">
                    <div className="card-body">
                        <Form onSubmit={onNewAddress} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Address
