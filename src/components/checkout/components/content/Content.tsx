import React from 'react'

function Content({ children }: React.PropsWithChildren<{}>) {
    return (
        <section className="section-gap">
            <div className="container">
                <div className="row">
                    {children}
                </div>
            </div>
        </section>
    )
}

export default Content
