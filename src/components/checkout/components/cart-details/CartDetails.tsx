import React from 'react'


import ICart from '@contracts/ICart'
import ICoupon from '@contracts/ICoupon'
import CurrencyService from '@services/currencyService'
import { amountWithDiscount, calculateDiscountAmount } from '@services/discountService'

type CartDetailsProps = {
    items: ICart[];
    coupon: ICoupon | null;
}

function CartDetails({ items, coupon }: CartDetailsProps) {

    const finalPrice = (price:number, discountedPrice:number, quantity: number) => {
        return discountedPrice ? discountedPrice * quantity : price * quantity
    }

    const totalCart = items.reduce((total, item) => {
        const price = item.discountedPrice ? item.discountedPrice : item.price;
        return total + (price * item.quantity)
    }, 0)

    const renderCartItems = items.map((item) => (
        <li key={item.productID} className="list-group-item d-flex justify-content-between lh-condensed">
            <div>
                <h6 className="my-0">{item.productTitle}</h6>
                <small className="text-muted">
                    تعداد ({item.quantity})
                </small>
            </div>
            <span className="text-muted">
                {CurrencyService.formatIRR(finalPrice(item.price, item.discountedPrice, item.quantity))}
            </span>
        </li>
    ))

    return (
        <div className="col-md-4 order-md-2 mb-4">
            <h5 className="d-flex justify-content-between align-items-center mb-4">
                <span className="text-muted">سبد خرید شما</span>
                <span className="badge badge-dark badge-pill px-3">{items.length}</span>
            </h5>
            <ul className="list-group mb-3">
                {renderCartItems}
                {coupon && (
                    <li className="list-group-item d-flex justify-content-between bg-light">
                        <div className="text-success">
                            <h6 className="my-0">کد تخفیف</h6>
                            <small>{coupon.code}</small>
                        </div>
                        <span className="text-success">{CurrencyService.formatIRR(calculateDiscountAmount(totalCart, coupon.percent))} -</span>
                    </li>
                )}
                <li className="list-group-item d-flex justify-content-between">
                <span>مجموع (ریال)</span>
                <strong>{CurrencyService.formatIRR(amountWithDiscount(totalCart, coupon ? coupon.percent : 0))}</strong>
                </li>
            </ul>
        </div>
    )
}

export default CartDetails
