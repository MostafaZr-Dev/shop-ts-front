import React, { useState, useEffect } from 'react'

import IMethodsList from '@components/checkout/IMethodsList'
import useAppState from 'state/useAppState'

type PaymentProps = {
    methods : IMethodsList[]
}

function Payment({ methods } : PaymentProps) {
    const [paymentMethod, setPaymentMethod] = useState<string>('')

    const { dispatch } = useAppState()

    const handleChangeMethod = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPaymentMethod(e.target.value)
        dispatch({
            type: "UPDATE_PAYMETN_METHOD",
            payload: {
                paymentMethod: e.target.value
            }
        })
    }

    const handleChangeGateway = (e: React.ChangeEvent<HTMLInputElement>) => {
        dispatch({
            type: "UPDATE_PAYMETN_GATEWAY",
            payload: {
                paymentGateway: e.target.value
            }
        })
    }

    const selectedMethod = methods.filter(method => paymentMethod === method.name).pop() 
    
    return (
        <div className="card py-4 px-2">
            <form>
                <div className="form-group">
                    {methods.map((method, index) => (
                        <div key={`method-${index}`} className="custom-control custom-radio custom-control-inline mb-3">
                            <input
                                type="radio"
                                id={method.name}
                                name='method'
                                className="custom-control-input"
                                value={method.name}
                                onChange={handleChangeMethod}
                            />
                            <label className="custom-control-label" htmlFor={method.name}>
                                {method.title}
                            </label>
                        </div>
                    ))}
                </div>
                <div className="form-group">
                    {selectedMethod && selectedMethod.gateways && (
                        <>
                            {selectedMethod.gateways.map((gateway, index) => (
                                <div key={`gateway-${index}`} className="custom-control custom-radio custom-control-inline mb-3">
                                    <input
                                    type="radio"
                                    id={gateway.name}
                                    name='gateway'
                                    className="custom-control-input"
                                    value={gateway.name}
                                    onChange={handleChangeGateway}
                                    />
                                    <label className="custom-control-label" htmlFor={gateway.name}>
                                        {gateway.title}
                                    </label>
                                </div>
                            ))}
                        </>
                    )}
                </div>
            </form>
        </div>
    )
}

export default Payment
