
type Gateways = {
    title: string;
    name: string;
}

export default interface IMethodsList {
    name: string,
    title: string,
    gateways: Gateways[] | null
}