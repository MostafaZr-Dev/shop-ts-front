import React from 'react'

type FilterWidgetProps = {
    title:string;
    children: React.ReactNode;
}

function FilterWidget({ title, children }: FilterWidgetProps) {
    return (
        <div className='card mb-3'>
            <div className='card-body'>
                <h5 className='mb-4 pb-2 border-bottom'>{title}</h5>
                <div>
                    {children}
                </div>
            </div>
        </div>
    )
}

export default FilterWidget
