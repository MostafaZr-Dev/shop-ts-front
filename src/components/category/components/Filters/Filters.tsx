import React from 'react'
import { useRouter } from 'next/router'

import ICategory from '@contracts/ICategory'
import FilterWidget from './FilterWidget'
import FilterItem from './FilterItem'
import { addQueryArg, removeQueryArg } from '@services/queryString'

type FiltersProps = {
    category: ICategory
}

function Filters({ category }: FiltersProps) {
    const router = useRouter()
    
    const filterProducts = (e:React.ChangeEvent<HTMLInputElement>, groupTitle: string, attrSlug: string) => {
        let query: string = ''
        
        query = e.target.checked 
        ?  addQueryArg(router.query, groupTitle, attrSlug) 
        : removeQueryArg(router.query, groupTitle, attrSlug)
        
        router.push({
            pathname: router.pathname,
            query
        })

    }

    return (
        <div className='col-md-3'>
            <div className='card'>
                <div className='card-body'>
                    {category.groups.map((group, index) => (
                        <FilterWidget key={`cat-gp-${index}`} title={group.title}>
                            <ul className="list-group">
                                {group.attributes.map((attr, index) => (
                                    <FilterItem 
                                        key={`attr-${index}`} 
                                        title={attr.title} 
                                        name={attr.slug} 
                                        onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                                            filterProducts(e, group.title, attr.slug)
                                        }}
                                    />
                                ))}
                            </ul>
                        </FilterWidget>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Filters
