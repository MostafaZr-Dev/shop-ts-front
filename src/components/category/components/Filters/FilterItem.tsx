import React from 'react'

type FilterItemProps = {
    title: string;
    name: string;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

function FilterItem({ title, name, onChange }: FilterItemProps) {
    
    return (   
        <li className="list-group-item">
            <div className="custom-control custom-checkbox mb-3">
                <input
                    type="checkbox"
                    className="custom-control-input"
                    id={name}
                    name={name}
                    onChange={onChange}
                />
                <label className="custom-control-label" htmlFor={name}>
                    {title}
                </label>
            </div>
        </li>
    )
}

export default FilterItem
