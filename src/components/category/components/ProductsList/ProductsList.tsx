import React from 'react'

import IProduct from '@contracts/IProduct'
import ProductItem from './ProductItem'
import useAppState from 'state/useAppState'

type ProductsListProps = {
    products: IProduct[]
}

function ProductsList({ products }: ProductsListProps) {

    const { dispatch } = useAppState()

    const addToCart = (product: IProduct) => {
        dispatch({
            type: 'ADD_TO_CART',
            payload: {
                item: {
                    productTitle: product.title,
                    productID: product.id,
                    productThumbnail: product.thumbnail,
                    price: product.price,
                    discountedPrice: product.discountedPrice,
                    quantity: 1
                }
            }
        })
    }

    const renderProducts = products.map((product) => (
        <ProductItem key={product.id} product={product} onAddToCart={(e) => {
            addToCart(product)
        }}/>
    ))

    return (
        <div className='col-md-9'>
            <div className="row justify-content-center">
                {!(products.length > 0) && (
                    <div className="card">
                        <div className="card-body">
                            <h6>محصولی پیدا نشد</h6>
                        </div>
                    </div>
                )}
                {products.length > 0 && <> {renderProducts} </>}
            </div>
        </div>
    )
}

export default ProductsList
