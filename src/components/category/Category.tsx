import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'

import ICategory from '@contracts/ICategory'
import IProduct from '@contracts/IProduct'
import Filters from './components/Filters'
import ProductsList from './components/ProductsList'
import useGetAPI from '@hooks/useGetAPI'

type CategoryProps = {
    products: IProduct[],
    category: ICategory
}

const getQueryParams = () => {
    const isClient = typeof window !== 'undefined' ? true : false
    
    if(isClient){
        const url = new URL(window.location.href)

        return url.search
    }

    return ''
}

function Category({ products, category }: CategoryProps) {
    const [productList, setProductList] = useState<IProduct[]>(products)
    
    const router = useRouter()
    const {slug} = router.query
    
    const queryString = getQueryParams()

    const [ filterProductsResponse, filterProductsAPI ] = useGetAPI({
        url:`/api/v1/categories/${slug}/products?${queryString}`,
        headers: {}
    })

    const { isLoading, success, error, data } = filterProductsResponse

    console.log(router.query.toString())

    useEffect(() => {
        filterProductsAPI<{products: IProduct[]}>()
    }, [router.query])

    useEffect(() => {
        if(success){
            setProductList(data.products)
        }
    }, [success])


    return (
        <div className="component-section bg-gray mt-5">
            <div className="container">
                <div className="row justify-content-center">
                        <Filters category={category}/>
                        {isLoading && (
                            <div className='col-md-9 justify-content-center'>
                                <div className="spinner-cmp"></div>
                            </div>
                        )}
                        {!isLoading && <ProductsList products={productList} />}
                </div>
            </div>
        </div>
    )
}

export default Category
