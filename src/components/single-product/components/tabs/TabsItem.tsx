import React from 'react'

function TabsItem() {
    return (
        <ul className="nav nav-line mb-md-5 mb-3" role="tablist">
            <li className="nav-item">
                <a className="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">توضیحات</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" id="attributes-tab" data-toggle="tab" href="#attributes" role="tab" aria-controls="attributes" aria-selected="false">مشخصات</a>
            </li>
            <li className="nav-item">
                <a className="nav-link" id="comments-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments" aria-selected="false">نظرات</a>
            </li>
        </ul>
    )
}

export default TabsItem
