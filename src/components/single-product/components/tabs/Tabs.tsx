import React from 'react'

import TabsItem from './TabsItem'
import Description from './TabsPane/Description'
import Attributes from './TabsPane/Attributes'
import Comments from './TabsPane/Comments'
import IComment from '@contracts/IComment'
import IProductAttributeGroup from '@contracts/IProductAttributeGroup'

type TabProps = {
    comments : IComment[];
    attributes: IProductAttributeGroup[];
}

function Tabs({ comments, attributes }: TabProps) {
    return (
        <div className="row justify-content-center ">
            <div className="col-md-12">
                <TabsItem />
                <div className="tab-content text-left">
                    <Description />
                    <Attributes attributes={attributes}/>
                    <Comments comments={comments}/>
                </div>
            </div>
        </div>
    )
}

export default Tabs
