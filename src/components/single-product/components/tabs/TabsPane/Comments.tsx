import React from 'react'
import Image from 'next/image'

import IComment from '@contracts/IComment'
import LangService from '@services/langService'

type CommentProps = {
    comments: IComment[]
}

function Comments({ comments }:CommentProps) {
    const renderComments = comments.map((comment) => (
        <li key={comment.id} className="comment ">
            <article className="comment-body">
                <footer className="comment-meta">
                    <div className="comment-author">
                        <Image alt='user-img' src={comment.user.avatar} width={100} height={100} />
                        <b className="fn">
                            <a href="#" rel="external nofollow" className="url">
                                {comment.user.firstName} {comment.user.lastName}
                            </a>
                        </b>
                        <span className="says">گفته:</span>
                    </div>
                    <div className="comment-metadata">
                        <a href="#">
                            <time dateTime="2018-09-02T12:17:07+00:00">
                                {LangService.toPersianNumber(comment.createdAt)}
                            </time>
                        </a>
                    </div>
                </footer>
                <div className="comment-content">
                    <p>
                        {comment.title}
                    <br />
                        {comment.body}
                    </p>
                </div>
                <div className="reply">
                    <span className="review-rating text-muted">
                        <i className="fa fa-star" />
                        <i className="fa fa-star" />
                        <i className="fa fa-star" />
                        <i className="fas fa-star-half-alt" />
                        <i className="far fa-star" />
                    </span>
                </div>
            </article>
        </li>
    ))
    return (
        <div
            className="tab-pane fade single-post"
            id="comments"
            role="tabpanel"
            aria-labelledby="comments-tab"
        >
            <div className="comments">
                <ul>
                    {comments.length === 0 && (
                        <li className="alert alert-warning alert-dismissible fade show" role="alert">
                        نظری ثبت نشده است
                        </li>
                    )}
                    {comments.length > 0 && (
                        <>
                            {renderComments}
                        </>
                    )}
                </ul>
            </div>
            <a href="javascript:;" className="btn btn-theme">
                یک بررسی اضافه کنید
            </a>
        </div>
    )
}

export default Comments
