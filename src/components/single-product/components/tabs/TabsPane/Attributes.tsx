import React from 'react'

import IProductAttributeGroup from '@contracts/IProductAttributeGroup'

type AttributesProps = {
    attributes: IProductAttributeGroup[]
}

function Attributes({ attributes }:AttributesProps) {
    return (
        <div className="tab-pane fade" id="attributes" role="tabpanel" aria-labelledby="attributes-tab">
            {attributes.map((attrGroup, index) => (
                <div key={`attr-g-${index}`} className="attributes-group">
                    <h6 className='mb-3'>
                        {attrGroup.title}
                    </h6>
                    <div className="table-responsive">
                        <table className="table table-striped table-bordered">
                            <tbody>
                                {attrGroup.attributes.map((attrItem, index) => (
                                    <tr key={`attr-item-${index}`}>
                                        <td>{attrItem.title}</td>
                                        <td>{attrItem.value}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default Attributes
