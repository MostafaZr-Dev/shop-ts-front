import React from 'react'
import Image from 'next/image'

type GalleryProps = {
    images: string[]
}

function Gallery({ images }: GalleryProps) {
    images = [...images, ...images, ...images]
    const renderImages = images.map((image, i) => (
        <div key={i} className="item">
            <a href="#" className="card border-0">
                <Image
                    className="card-img rounded"
                    src={image}
                    alt="product-gallery-image"
                    width={800}
                    height={800}
                />
            </a>
        </div>
    ))
    return (
        <div className="col-md-6">
            <div
                className="owl-carousel owl-theme dot-style-1 nav-round"
                data-items="[1,1]"
                data-margin={30}
                data-autoplay="true"
                data-loop="true"
                data-nav="true"
                data-dots="true"
            >
                {renderImages}
            </div>
        </div>
    )
}

export default Gallery
