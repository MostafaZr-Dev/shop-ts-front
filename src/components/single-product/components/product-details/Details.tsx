import React, { useEffect, useCallback, useState} from 'react'
import * as _ from 'lodash'

import IProduct from '@contracts/IProduct'
import CurrencyService from '@services/currencyService'
import LangService from '@services/langService'
import useAppState from 'state/useAppState'

type DetailsProps = {
    product: IProduct;
}

function Details({ product }: DetailsProps) {
    const [quantity, setQuantity] = useState<number>(1)
    const [variations, setVariations] = useState<object[]>([])
    const [price, setPrice] = useState<{main: number, discounted: number}>({
        main: product.price,
        discounted: product.discountedPrice
    })

    const { dispatch } = useAppState()

    useEffect(() => {
        const checkPriceVariations = () => {
            const matchedVariations = product.priceVariations.filter((priceVariation) => {
                return _.isEmpty(_.xorWith(priceVariation.items, variations, _.isEqual))
            })

            if(matchedVariations.length > 0){
                setPrice({
                    main: matchedVariations[0].price,
                    discounted: 0
                })
            }

        }

        checkPriceVariations()
    }, [variations, product.priceVariations])

    const addToCart = useCallback(
        () => {
            dispatch({
                type:'ADD_TO_CART',
                payload: {
                    item: {
                        productTitle: product.title,
                        productID: product.id,
                        productThumbnail: product.thumbnail,
                        price: price.main,
                        discountedPrice: price.discounted,
                        variations,
                        quantity
                    }
                }
            })
        },
        [
            price.main,
            price.discounted,
            product.id,
            quantity, 
            product.title, 
            product.thumbnail, 
            dispatch,
            variations
        ],
    )

    const isSpecialOffer = useCallback(
        (price: number, discountedPrice: number): boolean => {
            return (discountedPrice && discountedPrice > 0 && discountedPrice < price) as boolean
        },
        [],
    )

    const changeItemQuantity = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            const value = parseInt(e.target.value)
            setQuantity(value)
        },
        [],
    )

    const selectVariation = (e:React.ChangeEvent<HTMLSelectElement>, title: string) => {
        if(e.target.value === '0' ){
            return
        }

        setVariations(prev => ([
            ...prev,
            {
                [title]: e.target.value
            }
        ]))
    }
    console.log(variations)
    return (
        <div className="col-md-5">
            <h3 className="mb-3">{product.title}</h3>
            <div className="price mb-4">
                {isSpecialOffer(price.main, price.discounted) && (
                    <>
                        <del className="text-muted mr-2">
                            <span className="h6">
                                {CurrencyService.formatIRR(price.main)}
                            </span>
                        </del>
                        <span className="h5">{CurrencyService.formatIRR(price.discounted)}</span>
                    </>
                )}
                {!isSpecialOffer(price.main, price.discounted) && (<span className="h6">{CurrencyService.formatIRR(price.main)}</span>)}
            </div>
            <p className="text-muted">
                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از
                طراحان گرافیک استلورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ
                و با استفاده از طراحان گرافیک استلورم ایپسوم متن ساختگی با تولید سادگی
                نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است
            </p>
            {product.stock > 0 && (
                <>
                    {product.variations.map((variation, index) => (
                        <div key={`variation-${index}`} className="form-group">
                            <select className="custom-select" onChange={(e:React.ChangeEvent<HTMLSelectElement>) => selectVariation(e, variation.title,)}>
                                <option selected value={0}>{variation.title}</option>
                                {variation.items.map((item, index) => (
                                    <option key={`var-item-${index}`} value={item.value}>{item.title}</option>
                                ))}
                            </select>
                        </div>
                    ))}
                    <form className="form-inline my-lg-5 mb-4">
                        <input
                            type="number"
                            min={1}
                            max={product.stock}
                            className="form-control form-qty mr-2 w-25"
                            placeholder={`1`}
                            onChange={changeItemQuantity}
                            />
                        <button type='button' className="btn btn-primary" onClick={addToCart}>
                            <i className="fa fa-shopping-cart pr-3" />
                            افزودن به سبد خرید
                        </button>
                    </form>
                </>
            )}
            <div className="card border-0 font-size-14">
                <div>
                    <strong className="pr-2">تعداد:</strong> {LangService.toPersianNumber(product.stock)}
                </div>
                <div>
                    <strong className="pr-2">دسته:</strong>{" "}
                    <a href="#" rel="tag">
                        {product.category.title}
                    </a>
                    .
                </div>
            </div>
        </div> 
    )
}

export default Details
