import React from 'react'

import IProduct from '@contracts/IProduct'
import Details from './Details'
import Gallery from './Gallery'

type ProductDetailsProps = {
    product: IProduct,
}

function ProductDetails({product}: ProductDetailsProps) {
    
    return (
        <div className="row justify-content-between mb-lg-5 mb-4">
            <Gallery images={product.gallery} />
            <Details product={product} /> 
        </div>
    )
}

export default ProductDetails
