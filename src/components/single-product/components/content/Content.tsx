import React from 'react'

function Content({children}: React.PropsWithChildren<{}>) {
    return (
        <section className="section-gap">
            <div className="container">
                {children}
            </div>
        </section>
    )
}

export default Content
