import React, { useCallback } from 'react'

import Header from './components/header'
import Content from './components/content'
import ProductDetails from './components/product-details'
import Tabs from './components/tabs'
import IProduct from '@contracts/IProduct'
import IComment from '@contracts/IComment'
import useAppState from 'state/useAppState'

type SingleProductProps = {
    product: IProduct;
    comments : IComment[];
}

function SingleProduct({product, comments}:SingleProductProps) {

    const { dispatch } = useAppState()

    const addToCart = useCallback(
        (quantity: number) => {
            dispatch({
                type:'ADD_TO_CART',
                payload: {
                    item: {
                        productTitle: product.title,
                        productID: product.id,
                        productThumbnail: product.thumbnail,
                        price: product.price,
                        discountedPrice: product.discountedPrice,
                        quantity
                    }
                }
            })
        },
        [product.id, product.title, product.thumbnail, product.price, product.discountedPrice, dispatch],
    )

    return (
            <>
                <Header title={product.title}/>
                <Content>
                    <ProductDetails product={product} />
                    <Tabs comments={comments} attributes={product.attributes}/>
                </Content>
            </>
    )
}

export default SingleProduct
