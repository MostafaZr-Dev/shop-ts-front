import React, { useState, useEffect } from 'react'

import IOrder from '@contracts/IOrder'
import OrdersList from './components/orders-list'
import useGetAPI from '@hooks/useGetAPI'
import { TableSkeleton } from '@components/skeleton';

function Orders() {
    const [orders, setOrders] = useState<IOrder[]>([])

    const [getOrdersResponse, getOrdersFromAPI] = useGetAPI({
        url: '/api/v1/orders',
        headers : {
            'Authorization': `Bearer ${typeof window !== 'undefined' ? localStorage.getItem('token') : ''}`
        }
    })

    const { isLoading, success, error, data } = getOrdersResponse

    useEffect(() => {
        getOrdersFromAPI<{success: boolean, orders: IOrder[]}>()
    }, [])

    useEffect(() => {
        if(success){
            setOrders(data.orders)
        }
    }, [success])

    return (
        <>
            {isLoading && <TableSkeleton />}
            {!isLoading && <OrdersList items={orders}/>}
        </>
    )
}

export default Orders
