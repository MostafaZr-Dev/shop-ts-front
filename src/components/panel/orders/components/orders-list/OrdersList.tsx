import React from 'react'

import IOrder from '@contracts/IOrder'
import OrderItem from './OrderItem'

type OrdersListProps = {
    items: IOrder[];
}

function OrdersList({ items }: OrdersListProps) {

    const renderOrderItems = items.map((item) => (
        <OrderItem key={item.id} order={item} />
    ))

    return (
        <div className="table-responsive">
            <table className="table vl-custom-table">
                <thead>
                <tr>
                    <th className='text-center'>شناسه</th>
                    <th className='text-center'>مبلغ کل</th>
                    <th className='text-center'>مبلغ نهایی</th>
                    <th className='text-center'>تاریخ ایجاد</th>
                    <th className='text-center'>تاریخ بروزرسانی</th>
                    <th className='text-center'>وضعیت</th>
                </tr>
                </thead>
                <tbody>
                    {items.length === 0 && (
                        <tr>
                            <td colSpan={6} className='alert alert-warning text-center'>
                                <span>سفارشی ثبت نکرده اید</span>
                            </td>
                        </tr>
                    )}
                    {items.length > 0 && (
                        <> {renderOrderItems} </>
                    )}
                </tbody>
            </table>
        </div>
    )
}

export default OrdersList
