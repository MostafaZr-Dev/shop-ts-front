import React from 'react'

import IOrder from '@contracts/IOrder'
import CurrencyService from '@services/currencyService'
import LangService from '@services/langService'
import OrderStatus from './OrderStatus'

type OrderItemProps = {
    order: IOrder;
}

function OrderItem({ order }: OrderItemProps) {
    return (
        <tr>
            <td className='text-center'>#</td>
            <td className='text-center'>
                {CurrencyService.formatIRR(order.totalPrice)}
            </td>
            <td className='text-center'>
                {CurrencyService.formatIRR(order.finalPrice)}
            </td>
            <td className='text-center'>
                {LangService.toPersianNumber(order.createdAt)}
            </td>
            <td className='text-center'>
                {LangService.toPersianNumber(order.updatedAt)}
            </td>
            <td className='text-center'>
                <OrderStatus status={order.status} />
            </td>
        </tr>
    )
}

export default OrderItem
