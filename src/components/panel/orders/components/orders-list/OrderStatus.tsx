import React from 'react'

import OrderStatusType from '@contracts/OrderStatus'

type OrderStatusProps = {
    status: OrderStatusType
}

function OrderStatus({ status }: OrderStatusProps) {
    return (
        <>
            {status === OrderStatusType.INIT && <h6 className='alert alert-warning'>در حال پرداخت</h6>}
            {status === OrderStatusType.PAID && <h6 className='alert alert-success'>پرداخت شده</h6>}
        </>
    )
}

export default OrderStatus
