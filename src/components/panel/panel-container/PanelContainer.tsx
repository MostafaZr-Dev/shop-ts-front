import React from 'react'

type PanelContainerProps = {
    title: string;
    children: React.ReactNode;
}

function PanelContainer({title, children}: PanelContainerProps) {
    return (
        <div>
            <h6 className="mb-3 pb-3 border-bottom">{title}</h6>
            {children}
        </div>
    )
}

export default PanelContainer
