import React, { useState, useEffect, useCallback } from 'react'
import Image from 'next/image'
import Link from 'next/link'
import { debounce } from 'lodash'

import LoginFormValues from '@components/login/LoginFormValues'
import FormSchema from './FormSchema'

type FormErrors = {
    [key: string]: string
}

type FormProps = {
    onSubmit: (values: LoginFormValues) => void
}

function Form({ onSubmit }:FormProps) {

    const [formValues, setformValues] = useState<LoginFormValues>({
        email: '',
        password: ''
    })
    const [formErrors, setFormErrors] = useState<FormErrors | null>(null)
    
    const handleSubmit = useCallback(
        () => {
            if(formErrors){
                return
            }
    
    
            onSubmit(formValues)
        },
        [formErrors, formValues, onSubmit],
    )

    useEffect(() => {
        const validateFormData = (values: object) => {
            FormSchema.validate(values ,{ abortEarly: false }).then(values => {
                setFormErrors(null)
            })
            .catch(error => {
                const errors = error.inner.reduce((formError: object,innerError: any) => {
                    return {
                        ...formError,
                        [innerError.path] : innerError.message
                    }
                }, {})
    
                setFormErrors(errors)
    
            })
        }

        validateFormData(formValues)

    }, [formValues])

    

    const handleChange = debounce((e:React.ChangeEvent<HTMLInputElement>) => {
        setformValues(prevValues => ({
            ...prevValues,
            [e.target.name] : e.target.value
        }))
    } , 500)


    return (
        <div className="card-body d-flex align-items-center col-lg-5 p-md-5 p-3">
            <div className="w-100">
                <Image
                    className="mb-lg-5 mb-4"
                    src="/static/assets/img/logo-dark.png"
                    // srcSet="/static/assets/img/logo-dark@2x.png 2x"
                    alt='login-img'
                    width={76}
                    height={28}
                />
                <form>
                <div className="form-group">
                    <input 
                        name='email'
                        type="email" 
                        className="form-control" 
                        placeholder="آدرس ایمیل"
                        onChange={handleChange} 
                    />
                    <p className='mt-1 text-danger'>{formErrors && formErrors.email && (
                        <small>{formErrors.email}</small>
                    )}</p>
                </div>
                <div className="form-group">
                    <div className="icon-field-right">
                    <i className="fa fa-eye" />
                    <input
                        name='password'
                        type="password"
                        className="form-control"
                        placeholder="رمز عبور"
                        onChange={handleChange} 
                    />
                    <p className='mt-1 text-danger'>{formErrors && formErrors.password && (
                        <small>{formErrors.password}</small>
                    )}</p>
                    </div>
                </div>
                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                    <input
                        type="checkbox"
                        className="custom-control-input"
                        id="customCheck1"
                    />
                    <label className="custom-control-label" htmlFor="customCheck1">
                        مرا به خاطر بسپار
                    </label>
                    <a href="#" className="text-dark float-right">
                        رمز عبور فراموش شده؟
                    </a>
                    </div>
                </div>
                <div className="form-group">
                    <a className="btn btn-theme" onClick={handleSubmit}>
                    ورود
                    </a>
                </div>
                <div className="form-group mt-lg-5">
                    <Link href='/auth/register'>
                        <a>
                            ثبت نام
                        </a>
                    </Link>
                </div>
                </form>
            </div>
        </div>
    )
}

export default Form
