import Image from 'next/image'
import React from 'react'

function Details() {
    return (
        <div className="flex-column col-lg-7">
            <div className="position-relative">
                <Image
                    className="card-img-right flex-grow-1 "
                    src="/static/assets/img/cards/29a.jpg"
                    alt='login-details-img'
                    width={542}
                    height={460}
                />
                <div className="login-content">
                    <div className="h1 login-circle-logo font-weight-800 text-primary mb-4">
                        ک
                    </div>
                    <h2>آن را بهتر و سریعتر کنید</h2>
                    <p>کلاب بهترین است از نگاه مشتریان تم فارست</p>
                    <div className="row justify-content-center mt-lg-5">
                        <div className="col-md-8">
                        <ul className="list-group text-left">
                            <li className="list-group-item">
                            <i className="fa fa-check pr-3 text-primary font-size-12" />
                            برنامه ریزی ایده نوآوری و نسل
                            </li>
                            <li className="list-group-item">
                            <i className="fa fa-check pr-3 text-primary font-size-12" /> بزرگ
                            ارزش برند جهانی محصول است
                            </li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Details
