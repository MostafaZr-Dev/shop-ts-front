import React, { useEffect } from 'react'
import { useRouter } from "next/router";

import Container from './components/container'
import Form from './components/form'
import Details from './components/details'
import usePostAPI from '@hooks/usePostAPI'
import LoginFormValues from './LoginFormValues'

function Login() {
    const router = useRouter()

    const [loginResponse, loginRequestAPI] = usePostAPI({
        url:'/api/v1/auth/login'
    })

    const {isLoading, success, data} = loginResponse


    useEffect(() => {
        let timeout: ReturnType<typeof setTimeout>
        
        if(success) {
            localStorage.setItem('token', data.token)

            timeout = setTimeout(() => {
                router.push('/checkout')
            }, 1500)
        }

        return () => {
            clearTimeout(timeout)
        }
    }, [success])

    const login = (values: LoginFormValues) => {
        console.log({ values })

        loginRequestAPI(values)
    }

    return (
        <Container>
            <Form onSubmit={login}/>
            <Details />
        </Container>
    )
}

export default Login
