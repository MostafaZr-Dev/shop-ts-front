
export default interface LoginFormValues {

    email: string;
    password: string;
    firstName: string;
    lastName: string;

}