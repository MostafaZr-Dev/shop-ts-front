import React, { useEffect } from 'react'
import { useRouter } from "next/router";

import Container from './components/container'
import Form from './components/form'
import Details from './components/details'
import usePostAPI from '@hooks/usePostAPI'
import RegisterFormValues from './RegisterFormValues'

function Register() {
    const router = useRouter()

    const [registerResponse, registerRequestAPI] = usePostAPI({
        url:'/api/v1/auth/register'
    })

    const { isLoading, success,error , data } = registerResponse

    useEffect(() => {
        let timeout: ReturnType<typeof setTimeout>
        
        if(success) {
            timeout = setTimeout(() => {
                router.push('/auth/login')
            }, 1500)
        }

        return () => {
            clearTimeout(timeout)
        }
    }, [success])

    const register = (values: RegisterFormValues) => {
        console.log({ values })

        registerRequestAPI(values)
    }

    return (
        <Container>
            <Form onSubmit={register}/>
            <Details />
        </Container>
    )
}

export default Register
