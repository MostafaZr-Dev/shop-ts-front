import * as yup from 'yup'

const schema = yup.object().shape({
    email: yup.string().required('فیلد الزامی است!').email('ایمیل نامعتبر!'),
    password: yup.string().required('فیلد الزامی است!'),
    firstName: yup.string().required('فیلد الزامی است!'),
    lastName: yup.string().required('فیلد الزامی است!'),
})

export default schema

