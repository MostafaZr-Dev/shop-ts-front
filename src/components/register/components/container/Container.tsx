import React from 'react'

function Container({children}: React.PropsWithChildren<{}>) {
    return (
        <div className="section-gap">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-12">
                        <div className="card border-0 row no-gutters flex-column flex-md-row">
                            {children}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Container
