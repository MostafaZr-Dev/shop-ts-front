import React from 'react'

import Header from './components/header'
import Newest from './components/newest'
import BestSeller from './components/best-seller'
import MostViewed from './components/most-viewed'
import Populars from './components/populars'
import HomeData from '@contracts/HomeData'

type HomeProps = {
    data: HomeData
}

function Home({data} : HomeProps) {
    return (
        <>
            <Header title='صفحه اصلی فروشگاه'/>
            <Newest items={data.newests}/>
            <hr />
            <BestSeller items={data.bestSellers}/>
            <hr />
            <MostViewed items={data.mostViewed}/>
            <hr />
            <Populars items={data.populars}/>
        </>
    )
}

export default Home
