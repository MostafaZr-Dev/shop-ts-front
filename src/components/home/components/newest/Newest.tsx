import React from 'react'

import IProduct from '@contracts/IProduct'
import SectionItem from '../section-item'
import SectionContent from '../section-content'

type NewestProps = {
    items: IProduct[]
}

function Newest({items}: NewestProps) {

    const renderNewestItems = items.map((item) => (
        <SectionItem key={item.id} item={item} />
    ))
    
    return (
        <SectionContent title='جدیدترین محصولات'>                
            {renderNewestItems}
        </SectionContent>
    )
}

export default Newest
