import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

import IProduct from '@contracts/IProduct'
import CurrencyService from '@services/currencyService'
import useAppState from 'state/useAppState'

type SectionItemProps = {
    item: IProduct;
}

function SectionItem({ item }: SectionItemProps) {
    const isSpecialOffer = (price: number, discountedPrice: number): boolean => {
        return (discountedPrice && discountedPrice > 0 && discountedPrice < price) as boolean
    }

    const { dispatch } = useAppState()

    const addToCart = (product: IProduct) => {
        dispatch({
            type: 'ADD_TO_CART',
            payload: {
                item: {
                    productTitle: product.title,
                    productID: product.id,
                    productThumbnail: product.thumbnail,
                    price: product.price,
                    discountedPrice: product.discountedPrice,
                    quantity: 1
                }
            }
        })
    }

    return (
        <div className="col-md-4">
            <div className="card border-0 mb-4 box-hover">
                <div className="position-relative">
                    {isSpecialOffer(item.price, item.discountedPrice) && (
                        <div className="ft-tag ft-inside-tr">ویژه</div>
                    )}
                    <Image className="card-img-top" src={item.thumbnail} width={400} height={400} alt="item-image" />
                </div>
                <div className="card-body py-4 text-center">
                    <h6 className="mb-2 font-size-16">
                        <Link 
                            href={{
                                pathname: '/products/[id]',
                                query: { id: item.id },
                            }}
                        >
                            <a>{item.title}</a>
                        </Link>
                    </h6>
                    <div className="price mb-3">
                        {isSpecialOffer(item.price, item.discountedPrice) && (
                            <>
                                <del className="text-muted mr-2"><span className="font-size-14 h6">{CurrencyService.formatIRR(item.price)}</span></del>
                                <span className="h6">{CurrencyService.formatIRR(item.discountedPrice)}</span>
                            </>
                        )}
                        {!isSpecialOffer(item.price, item.discountedPrice) && (<span className="h6">{CurrencyService.formatIRR(item.price)}</span>)}
                    </div>
                    <a className="btn btn-sm btn-pill btn-outline" onClick={(e) => {
                        addToCart(item)
                    }}>افزودن به سبد خرید</a>
                </div>
            </div>
        </div>
    )
}

export default SectionItem
