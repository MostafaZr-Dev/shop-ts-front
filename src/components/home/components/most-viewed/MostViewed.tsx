import React from 'react'

import IProduct from '@contracts/IProduct'
import SectionItem from '../section-item'
import SectionContent from '../section-content'

type MostViewedProps = {
    items: IProduct[]
}

function MostViewed({ items }: MostViewedProps) {
    const renderMostViewedItems = items.map((item) => (
        <SectionItem key={item.id} item={item} />
    ))

    return (
        <SectionContent title='پربازدیدترین محصولات'>                
            {renderMostViewedItems}
        </SectionContent>
    )
}

export default MostViewed
