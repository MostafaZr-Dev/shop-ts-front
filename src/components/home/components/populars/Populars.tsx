import React from 'react'

import IProduct from '@contracts/IProduct'
import SectionItem from '../section-item'
import SectionContent from '../section-content'

type PopularsProps = {
    items: IProduct[]
}

function Populars({ items }: PopularsProps) {
    const renderPopularsItems = items.map((item) => (
        <SectionItem key={item.id} item={item} />
    ))

    return (
        <SectionContent title='محبوب ترین محصولات'>                
            {renderPopularsItems}
        </SectionContent>
    )
}

export default Populars
