import React from 'react'

type SectionContentProps = {
    title: string;
    children: React.ReactNode;
}

function SectionContent({title, children}: SectionContentProps) {
    return (
        <>
            <div className="row justify-content-center textcenter py-4">
                <h6 className="mb-0 py-4 px-4 bg-white">{title}</h6>
            </div>  
            <div className='component-section py-0'>
                <div className="container">
                    <div className="row justify-content-center">                 
                        {children}
                    </div>
                </div>
            </div>
        </>
    )
}

export default SectionContent
