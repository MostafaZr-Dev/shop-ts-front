import React from 'react'

import IProduct from '@contracts/IProduct'
import SectionItem from '../section-item'
import SectionContent from '../section-content'

type BestSellerProps = {
    items: IProduct[]
}

function BestSeller({items} : BestSellerProps) {
    const renderBestSellerItems = items.map((item) => (
        <SectionItem key={item.id} item={item} />
    ))
    return (
        <SectionContent title='پرفروش ترین محصولات'>              
            {renderBestSellerItems}
        </SectionContent>   
    )
}

export default BestSeller
