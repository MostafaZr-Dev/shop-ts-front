import IProduct from '@contracts/IProduct'
import React from 'react'

import Header from './components/header'
import ProductsList from './components/products-list'

type ProductsProps = {
    products: IProduct[]
}

function Products({products}:ProductsProps) {
    return (
        <>  
            <Header />
            <ProductsList products={products} />  
        </>
    )
}

export default Products
