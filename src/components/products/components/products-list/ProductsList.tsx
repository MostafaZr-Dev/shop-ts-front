import React from 'react'

import ProductItem from './ProductItem'
import IProduct from '@contracts/IProduct'
import useAppState from 'state/useAppState'

type ProductsListProps = {
    products: IProduct[]
}

function ProductsList({products}: ProductsListProps) {

    const { dispatch } = useAppState()

    const addToCart = (product: IProduct) => {
        dispatch({
            type: 'ADD_TO_CART',
            payload: {
                item: {
                    productTitle: product.title,
                    productID: product.id,
                    productThumbnail: product.thumbnail,
                    price: product.price,
                    discountedPrice: product.discountedPrice,
                    quantity: 1
                }
            }
        })
    }

    const renderProducts = products.map((product) => (
        <ProductItem key={product.id} product={product} onAddToCart={(e) => {
            addToCart(product)
        }}/>
    ))
    return (
        <section className="section-gap">
            <div className="container">
                <div className="row justify-content-center">
                    {renderProducts} 
                </div>
            </div>
        </section>
    )
}

export default ProductsList
