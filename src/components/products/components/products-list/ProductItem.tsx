import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

import IProduct from '@contracts/IProduct'
import CurrencyService from '@services/currencyService'

type ProductItemProps = {
    product: IProduct
    onAddToCart: (e: React.MouseEvent) => void
}

function ProductItem({ product, onAddToCart }: ProductItemProps) {
    const isSpecialOffer = (price: number, discountedPrice: number): boolean => {
        return (discountedPrice && discountedPrice > 0 && discountedPrice < price) as boolean
    }

    return (
        <div className="col-md-3">
            <div className="card border-0 mb-4 box-hover">
                <div className="position-relative">
                    {isSpecialOffer(product.price, product.discountedPrice) && (
                        <div className="ft-tag ft-inside-tr">ویژه</div>
                    )}
                    <Image className="card-img-top" src={product.thumbnail} width={400} height={400} alt="product-image" />
                </div>
                <div className="card-body py-4 text-center">
                    <h6 className="mb-2 font-size-16">
                        <Link 
                            href={{
                                pathname: '/products/[id]',
                                query: { id: product.id },
                            }}
                        >
                            <a>{product.title}</a>
                        </Link>
                    </h6>
                    <div className="price mb-3">
                        {isSpecialOffer(product.price, product.discountedPrice) && (
                            <>
                                <del className="text-muted mr-2"><span className="font-size-14 h6">{CurrencyService.formatIRR(product.price)}</span></del>
                                <span className="h6">{CurrencyService.formatIRR(product.discountedPrice)}</span>
                            </>
                        )}
                        {!isSpecialOffer(product.price, product.discountedPrice) && (<span className="h6">{CurrencyService.formatIRR(product.price)}</span>)}
                    </div>
                    <a className="btn btn-sm btn-pill btn-outline" onClick={onAddToCart}>افزودن به سبد خرید</a>
                </div>
            </div>
        </div>
    )
}

export default ProductItem
