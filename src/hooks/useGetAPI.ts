import { useCallback, useState } from 'react'
import httpService from 'services/httpService'

type getAPIParams = {
  url: string;
  headers: object;
}

type getResponseType = {
  data: any;
  isLoading: boolean;
  success: boolean;
  error: boolean;
}

// eslint-disable-next-line no-unused-vars
const useGetAPI = ({ url, headers }: getAPIParams): [getResponseType, <T>() => void] => {
  const [res, setRes] = useState<getResponseType>({
    data: null,
    isLoading: false,
    success: false,
    error: false
  })

  const callGetAPI = useCallback(<T>() => {
    const configs = {
      headers: {
        ...headers
      }
    }

    setRes({
      data: null,
      success: false,
      error: false,
      isLoading: true
    })

    httpService
      .get<T>(url, configs)
      .then((response) => {
        setRes({
          isLoading: false,
          success: true,
          error: false,
          data: response.data
        })
      })
      .catch((error) => {
        console.log(error)
        setRes({
          isLoading: false,
          success: false,
          error: true,
          data: null
        })
      })
  }, [url, headers])

  return [res, callGetAPI]
}

export default useGetAPI
