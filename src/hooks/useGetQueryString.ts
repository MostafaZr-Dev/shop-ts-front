import { useState, useEffect, useCallback } from 'react'
import { useLocation, useHistory } from 'react-router-dom'

type QueryString = {
  [key: string]: string | string[] | object
}

type QueryData = {
  params: QueryString,
  query: string
}

const useQueryString = (): [QueryData, (key: string, value: string, deletedKey?: string | undefined) => void] => {
  const [query, setQuery] = useState<QueryData>({
    params: {},
    query: ''
  })

  const location = useLocation()
  const history = useHistory()

  const queryParams = new URLSearchParams(location.search)

  useEffect(() => {
    const values = Array.from(queryParams.entries())

    if (values.length > 0) {
      const queryString = values.reduce<QueryString>((result, query) => {
        if (query[0] in result) {
          return {
            ...result,
            [query[0]]: queryParams.getAll(query[0])
          }
        }

        return {
          ...result,
          [query[0]]: query[1]
        }
      }, {})

      setQuery({
        params: queryString,
        query: queryParams.toString()
      })
    }
  }, [location.search])

  const setQueryString = useCallback(
    (key: string, value: string, deletedKey?: string | undefined) => {
      if (deletedKey) {
        queryParams.delete(deletedKey)
      }
      queryParams.set(key, decodeURIComponent(value))
      history.push({
        search: `${queryParams.toString()}`
      })
    },
    []
  )

  return [query, setQueryString]
}

export default useQueryString
