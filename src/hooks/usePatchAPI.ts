import { useCallback, useState } from 'react'
import httpService from 'services/httpService'

type PatchAPIParams = {
  url: string;
  configs?: object;
}

type PatchAPIResponse = {
  data: any;
  isLoading: boolean;
  success: boolean;
  error: boolean;
}

// eslint-disable-next-line no-unused-vars
const usePatchAPI = ({ url, configs = {} }: PatchAPIParams): [PatchAPIResponse, <T, B>(data: B) => void] => {
  const [res, setRes] = useState<PatchAPIResponse>({
    data: null,
    isLoading: false,
    success: false,
    error: false
  })

  const callPatchAPI = useCallback(
    <T, B>(data: B) => {
      setRes((prevState) => ({
        data: null,
        isLoading: true,
        success: false,
        error: false
      }))

      httpService
        .patch<T, B>(url, data, configs)
        .then((response) => {
          setRes({
            isLoading: false,
            success: true,
            error: false,
            data: response.data
          })
        })
        .catch((error) => {
          setRes({
            isLoading: false,
            success: false,
            error: true,
            data: error.response.data.message
          })
          console.log(error)
        })
    },
    [url, configs]
  )

  return [res, callPatchAPI]
}

export default usePatchAPI
