import httpService from "./httpService"

export default class AuthService {


    public async check(): Promise<boolean> {
        const token = localStorage.getItem('token')

        if (!token) {
            return false
        }

        try {
            const validationResponse = await httpService.get<{ success: boolean }>('/api/v1/auth/check', {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })

            if (validationResponse.data.success) {
                return true
            }

            return false
        } catch (error) {
            console.log(error)

            return false
        }
    }

    public async login(email: string, password: string): Promise<any>{

        try {
            
        } catch (error) {
            
        }

    }

}