import HomeData from "@contracts/HomeData";
import ICategory from "@contracts/ICategory";
import IProduct from "@contracts/IProduct";
import httpService from "./httpService";


export default class ProductService {

    public async produtcs() {

        const produtcsResponse = await httpService.get<{ products: IProduct[] }>('/api/v1/products')

        if (produtcsResponse.data) {
            return produtcsResponse.data.products
        }
    }

    public async product(id: string) {
        const productResponse = await httpService.get<{ product: IProduct }>(`/api/v1/products/${id}`)

        if (productResponse.data) {
            return productResponse.data.product
        }
    }

    public async comments(id: string) {
        const productCommentsResponse = await httpService.get<{ comments: IProduct }>(`/api/v1/products/${id}/comments`)

        if (productCommentsResponse.data) {
            return productCommentsResponse.data.comments
        }
    }

    public async homeData() {
        const homeDataResponse = await httpService.get<HomeData>('/api/v1/home')

        if (homeDataResponse.data) {
            return homeDataResponse.data
        }
    }

    public async getCategories() {

        const productsCategoryResponse = await httpService.get<{ categories: ICategory[] }>(`/api/v1/categories`)

        if (productsCategoryResponse.data) {
            return productsCategoryResponse.data.categories
        }

    }

    public async getProductsCategory(slug: string) {

        const productsCategoryResponse = await httpService.get<{ products: IProduct[], category: ICategory }>(`/api/v1/categories/${slug}/products`)

        if (productsCategoryResponse.data) {
            return {
                products: productsCategoryResponse.data.products,
                category: productsCategoryResponse.data.category
            }
        }

    }

}