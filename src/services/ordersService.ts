import IOrder from '@contracts/IOrder'
import httpService from './httpService'

export default class OrdersService {

    public async getUserOrders() {

        const produtcsResponse = await httpService.get<{ orders: IOrder[] }>('/api/v1/orders', {
            headers: {
                'Authorization': `Bearer ${typeof window !== 'undefined' ? localStorage.getItem('token') : ''}`
            }
        })

        if (produtcsResponse.data) {
            return produtcsResponse.data.orders
        }

    }

}