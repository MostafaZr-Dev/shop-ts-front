
export const addQueryArg = (query: any, key: string, value: string): string => {
    const queryParams = new URLSearchParams(query)

    // key=value,value2,value3
    if (queryParams.has(key)) {
        const prevValue = queryParams.get(key)
        queryParams.set(key, decodeURIComponent(`${prevValue},${value}`))
    } else {
        queryParams.set(key, value)
    }

    return queryParams.toString()
}

export const removeQueryArg = (query: any, key: string, value: string): string => {
    const queryParams = new URLSearchParams(query)

    if (queryParams.has(key)) {
        const newValue = queryParams.get(key)?.split(',').filter(currentValue => currentValue !== value)

        if (newValue && newValue.length > 0) {
            queryParams.set(key, decodeURIComponent(`${newValue}`))
        } else {
            queryParams.delete(key)
        }

    }

    return queryParams.toString()
}