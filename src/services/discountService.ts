export const amountWithDiscount = (amount: number, percent: number) => {
    return (amount * (1 - (percent / 100)))
}

export const calculateDiscountAmount = (amount: number, percent: number) => {
    return amount - amountWithDiscount(amount, percent)
}
