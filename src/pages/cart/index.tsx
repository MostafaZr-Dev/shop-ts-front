import React, {useEffect} from 'react'

import { ShopLayout } from '@components/layout'
import Cart from '@components/cart'

function CartPage() {

    useEffect(() => {
        if(document){
            document.querySelector('body')?.classList.remove('bg-gray')
        }
    }, [])

    return (
        <ShopLayout title='فروشگاه اینترنتی | سبد خرید'>
            <Cart />
        </ShopLayout>
    )
}

export default CartPage
