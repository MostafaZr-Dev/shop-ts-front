import React from 'react'
import Document, {Html,Head,Main,NextScript} from 'next/document'

export default class ShopDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                    <meta charSet="utf-8" />
                    {/*favicon icon*/}
                    <link rel="icon" type="image/png" href="static/assets/img/favicon.png" />
                    {/*web fonts*/}
                    {/*basic styles*/}
                    <link href="/static/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
                    <link href="/static/assets/vendor/fontawesome/css/all.min.css" rel="stylesheet" />
                    <link href="/static/assets/vendor/custom-icon/style.css" rel="stylesheet" />
                    <link href="/static/assets/vendor/vl-nav/css/core-menu.css" rel="stylesheet" />
                    <link href="/static/assets/vendor/animate.min.css" rel="stylesheet" />
                    <link
                        href="/static/assets/vendor/magnific-popup/magnific-popup.css"
                        rel="stylesheet"
                    />
                    <link href="/static/assets/vendor/owl/assets/owl.carousel.min.css" rel="stylesheet" />
                    <link
                        href="/static/assets/vendor/owl/assets/owl.theme.default.min.css"
                        rel="stylesheet"
                    />
                    {/*custom styles*/}
                    <link href="/static/assets/css/main.css" rel="stylesheet" />
                    {/*[if (gt IE 9) |!(IE)]><!*/}
                    {/*<link rel="stylesheet" href="static/assets/vendor/custom-nav/css/effects/fade-menu.css"/>*/}
                    <link
                        rel="stylesheet"
                        href="/static/assets/vendor/vl-nav/css/effects/slide-menu.css"
                    />
                    <link
                        rel="stylesheet"
                        href="/static/assets/css/custom.css"
                    />
                    {/*<![endif]*/}
                    {/* Global site tag (gtag.js) - Google Analytics */}
                </Head>
                <body className='bg-gray'>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}
