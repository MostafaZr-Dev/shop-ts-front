import React, {useEffect} from 'react'
import { GetStaticProps, GetStaticPaths } from 'next'
import { ParsedUrlQuery } from 'querystring'

import { ShopLayout } from '@components/layout'
import SingleProduct from '@components/single-product'
import ProductsService from '@services/productsService'
import IProduct from '@contracts/IProduct'
import IComment from '@contracts/IComment'

type IParams = ParsedUrlQuery & {
    id: string
}

type SingleProductPageProps = {
    product : IProduct;
    comments : IComment[];
}

function SingleProductPage({ product, comments }:SingleProductPageProps) {

    useEffect(() => {
        if(document){
            document.querySelector('body')?.classList.remove('bg-gray')
        }
    }, [])

    return (
        <ShopLayout title={`فروشگاه اینترنتی | محصول تکی`}>
            <SingleProduct product={product} comments={comments}/>
        </ShopLayout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    const { id } = context.params as IParams
    
    const productsService = new ProductsService()

    const product = await productsService.product(id)
    const comments = await productsService.comments(id)

    if(!product){
        return {
            notFound: true
        }
    }

    return {
        props : {
            product,
            comments
        }
    }
}

export const getStaticPaths: GetStaticPaths = async () => {
    const productsService = new ProductsService()

    const products = await productsService.produtcs()

    const paths = products?.map(product => ({
        params: {
            id: product.id
        }
    }))

    return {
        paths: paths || [],
        fallback: true
    }
}

export default SingleProductPage
