import React from 'react'
import { GetStaticProps } from 'next'

import { ShopLayout } from "@components/layout";
import Products from '@components/products'
import ProductService from 'services/productsService';
import IProduct from '@contracts/IProduct';

type ProductsPageProps = {
    products: IProduct[]
}

function ProductsPage({ products }:ProductsPageProps) {  
    return (
        <ShopLayout title='فروشگاه اینترنتی | محصولات'>
            <Products products={products}/>
        </ShopLayout>
    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    const productService = new ProductService()
    const products = await productService.produtcs()

    return {
        props: {
            products: products || []
        }
    }
}

export default ProductsPage
