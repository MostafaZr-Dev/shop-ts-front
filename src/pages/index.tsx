
import { GetStaticProps } from 'next'

import { ShopLayout } from '@components/layout'
import Home from '@components/home'
import ProductService from '@services/productsService'
import HomeData from '@contracts/HomeData'

type HomePageProps = {
  homeData: HomeData
}

export default function HomePage({ homeData }: HomePageProps) {
  return (
    <ShopLayout title='فروشگاه اینترینتی | خانه'>
      <Home data={homeData}/>
    </ShopLayout>
  )
}


export const getStaticProps: GetStaticProps = async (context) => {
  const productService = new ProductService()
  const homeData = await productService.homeData()

  return {
      props: {
          homeData : homeData ? homeData : {}
      }
  }
}
