import React from 'react'

import { AuthLayout } from '@components/layout'
import Register from '@components/register'

function RegisterPage() {
    return (
        <AuthLayout title='فروشگاه اینترنتی | ثبت نام در سایت'>
            <Register />
        </AuthLayout>
    )
}

export default RegisterPage
