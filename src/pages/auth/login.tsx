import React from 'react'

import { AuthLayout } from '@components/layout'
import Login from '@components/login'

function LoginPage() {
    return (
        <AuthLayout title='فروشگاه اینترنتی | ورود به سایت'>
            <Login />
        </AuthLayout>
    )
}

export default LoginPage
