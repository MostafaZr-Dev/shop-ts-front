import React from 'react'

import { ShopLayout } from '@components/layout'
import Checkout from '@components/checkout'

function index() {
    return (
        <ShopLayout title={`فروشگاه اینترنتی | بررسی سفارش`}>
            <Checkout />
        </ShopLayout>
    )
}

export default index
