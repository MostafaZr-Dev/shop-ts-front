import React from 'react'
import Image from 'next/image'

function NotFoundPage() {
    return (
        <section className="section-gap section-top ">
            <div className="container">
                <div className="row justify-content-md-center align-items-center text-lg-left text-center">
                    <div className="col-md-4">
                        <Image 
                            className="mb-lg-0 mb-5" 
                            src="/static/assets/img/error-icon.png" 
                            width={290} 
                            height={300} 
                            alt="notfound-image" 
                        />
                    </div>
                    <div className="col-md-5 pl-lg-5">
                        <h1 className="font-size-60 text-primary">خطای 404</h1>
                        <p>با عرض پوزش صفحه پیدا نشد</p>
                        <a href="index-2.html" className="btn btn-pill btn-theme">برگشت به صفحه اصلی</a>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default NotFoundPage
