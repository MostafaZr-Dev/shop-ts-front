import React from 'react'

import { ShopLayout } from '@components/layout'
import PaymentVerify from '@components/payment-verify'

function PaymentVerifyPage() {
    return (
        <ShopLayout title="فروشگاه اینترنتی | تایید پرداخت">
            <PaymentVerify />
        </ShopLayout>
    )
}

export default PaymentVerifyPage
