import React from 'react'
import { GetStaticProps, GetStaticPaths } from 'next'
import { ParsedUrlQuery } from 'querystring'

import { ShopLayout } from '@components/layout'
import ProductsService from '@services/productsService'
import IProduct from '@contracts/IProduct'
import ICategory from '@contracts/ICategory'
import Category from '@components/category'

type IParams = ParsedUrlQuery & {
    slug: string
}

type ProductsCategoryProps = {
    products: IProduct[],
    category: ICategory
}

function ProductsCategory({ products, category }: ProductsCategoryProps) {
    return (
        <ShopLayout title='فروشگاه اینترنتی | محصولات دسته بندی'>
            <Category products={products} category={category} />
        </ShopLayout>
    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    const { slug } = context.params as IParams
    
    const productsService = new ProductsService()

    const response = await productsService.getProductsCategory(slug)

    return {
        props : {
            products: response?.products || [],
            category: response?.category || null
        }
    }
}


export const getStaticPaths: GetStaticPaths = async () => {
    const productsService = new ProductsService()

    const categories = await productsService.getCategories()

    const paths = categories?.map(category => ({
        params: {
            slug: category.slug
        }
    }))

    return {
        paths: paths || [],
        fallback: true
    }
}

export default ProductsCategory
