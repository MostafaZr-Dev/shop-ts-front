import React from 'react'
import { GetStaticProps } from 'next'

import { PanelLayout } from '@components/layout'
import PanelContainer from '@components/panel/panel-container'
import Orders from '@components/panel/orders'
import OrdersService from '@services/ordersService'
import IOrder from '@contracts/IOrder'


function OrdersPage() {
    return (
        <PanelLayout title="فروشگاه اینترنتی | پنل کاربری | لیست سفارش ها">
            <PanelContainer title='لیست سفارش ها'>
                <Orders />
            </PanelContainer>
        </PanelLayout>
    )
}

export default OrdersPage
