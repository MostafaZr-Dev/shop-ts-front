import React from 'react'

import { PanelLayout } from '@components/layout'
import PanelContainer from '@components/panel/panel-container'

function PanelPage() {
    return (
        <PanelLayout title="فروشگاه اینترنتی | پنل کاربری">
            <PanelContainer title="داشبورد">

            </PanelContainer>
        </PanelLayout>
    )
}

export default PanelPage
